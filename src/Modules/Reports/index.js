import React from 'react';

import {
  Route
} from 'react-router-dom';

import Reports from './Reports';
import Usage from './Usage';
import Errors from './Errors';

import {Card, CardText} from 'material-ui/Card';

export default class StoreDirectory extends React.Component {

  render() {
    return (
      <Card className="MainCard">
        <CardText style={{height: 'inherit', position: 'relative', padding: '0px'}}>
        <Route exact path={`${this.props.match.url}/`} component={Reports} />
        <Route path={`${this.props.match.url}/usage`} component={Usage} />
        <Route path={`${this.props.match.url}/errors`} component={Errors} />
      </CardText>
      </Card>
    );
  }
}