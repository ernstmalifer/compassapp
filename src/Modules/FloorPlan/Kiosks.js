import React from 'react';
import { Row, Col } from 'react-flexbox-grid';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import ContentAdd from 'material-ui/svg-icons/content/add';
import ActionZoomIn from 'material-ui/svg-icons/action/zoom-in';
import ActionZoomOut from 'material-ui/svg-icons/action/zoom-out';
import RaisedButton from 'material-ui/RaisedButton';
import Delete from 'material-ui/svg-icons/action/delete';

import 'whatwg-fetch';

import {
    Table,
    TableBody,
    TableRow,
    TableRowColumn,
} from 'material-ui/Table';
import Popover from 'material-ui/Popover';
import Divider from 'material-ui/Divider';
import Menu from 'material-ui/Menu';
import MenuItem from 'material-ui/MenuItem';

import SvgIcon from 'material-ui/SvgIcon';
import IconButton from 'material-ui/IconButton';

import AddAmenity from './Modules/AddAmenity.js';
import EditAmenity from './Modules/EditAmenity.js';

import AddKiosk from './Modules/AddKiosk.js';
import EditKiosk from './Modules/EditKiosk.js';

const EditStoreIcon = (props) => (
  <SvgIcon {...props}>
    <g transform="matrix(.75,0,0,.75,2,0)">
    <path d="M10.681 18.207l-2.209 5.67 5.572-2.307-3.363-3.363zM26.855 6.097l-0.707-0.707c-0.78-0.781-2.047-0.781-2.828 0l-1.414 1.414 3.535 3.536 1.414-1.414c0.782-0.781 0.782-2.048 0-2.829zM10.793 17.918l0.506-0.506 3.535 3.535 9.9-9.9-3.535-3.535 0.707-0.708-11.113 11.114zM23.004 26.004l-17.026 0.006 0.003-17.026 11.921-0.004 1.868-1.98h-14.805c-0.552 0-1 0.447-1 1v19c0 0.553 0.448 1 1 1h19c0.553 0 1-0.447 1-1v-14.058l-2.015 1.977 0.054 11.085z"></path>
    </g>
  </SvgIcon>
)

const RemoveStoreIcon = (props) => (
  <SvgIcon {...props}>
    <g transform="matrix(.6,0,0,.6,3,3)">
    <path d="M4 10v20c0 1.1 0.9 2 2 2h18c1.1 0 2-0.9 2-2v-20h-22zM10 28h-2v-14h2v14zM14 28h-2v-14h2v14zM18 28h-2v-14h2v14zM22 28h-2v-14h2v14z"></path>
    <path d="M26.5 4h-6.5v-2.5c0-0.825-0.675-1.5-1.5-1.5h-7c-0.825 0-1.5 0.675-1.5 1.5v2.5h-6.5c-0.825 0-1.5 0.675-1.5 1.5v2.5h26v-2.5c0-0.825-0.675-1.5-1.5-1.5zM18 4h-6v-1.975h6v1.975z"></path>
    </g>
  </SvgIcon>
)

// const tableData = [
//   {amenity_name: 'Toilets'}, 
//   {amenity_name: 'Parents Room'},
//   {amenity_name: 'Customer Lounge'},
//   {amenity_name: 'ATM'},
//   {amenity_name: 'Smoking Lounge'},
//   {amenity_name: 'Kainan'}
// ];

const kioskData = [
  {kiosk_name: 'TestKiosk', kioskID: '1'}, 
  {kiosk_name: 'Kiosk2', kioskID: '2'},
  {kiosk_name: 'Kiosk3', kioskID: '3'},
  {kiosk_name: 'Super Kiosk', kioskID: '4'}
];

import Kiosk from './Components/Box';
import Amenity from './Components/Box';
import Drop from './Components/Drop';

class Kiosks extends React.Component {

  state = {
    fixedHeader: true,
    fixedFooter: true,
    stripedRows: false,
    showRowHover: false,
    selectable: true,
    multiSelectable: false,
    enableSelectAll: false,
    deselectOnClickaway: true,
    showCheckboxes: false,
    floorplans: this.props.floorplans,
    currentLevel: this.props.floorplans.length ? this.props.floorplans.length : null,
    amenityPopover: this.props.amenityPopover,
    editKioskID: null,
    editAmenityID: null,
    dragging: false,
    showAddAmenity: false,
    showEditAmenity: false,
    kioskPopover: this.props.kioskPopover,
    draggingKiosk: false,
    showAddKiosk: false,
    showEditKiosk: false,
    kiosks: []
  };

  componentWillMount = () => {
    this.loadKiosks();
  }

  loadKiosks = () => {
    window.fetch(window.endpoint + '/kiosks').then((response) => {
      return response.text();
    })
    .then((body) => {
      const response = JSON.parse(body);
      if(response.status || response.status === false){}
      else {
        this.setState({kiosks: response.kiosks})
      }
    })
  }

  showAddAmenityModal = (event) => {
    this.setState({showAddAmenity:true});
  }

  hideAddAmenityModal = (event) => {
    this.setState({showAddAmenity:false});
  }

  showEditAmenityModal = (id) => {
    this.setState({editAmenityID: id, showEditAmenity:true})
  }
   
   
  hideEditAmenityModal = (id) => {
    this.setState({editAmenityID: null, showEditAmenity:false})
  }

  showAddKioskModal = (event) => {
    this.setState({showAddKiosk:true})
    this.loadKiosks();
  }

  hideAddKioskModal = (event) => {
    this.setState({showAddKiosk:false})
    this.loadKiosks();
  }

  showEditKioskModal = (id) => {
    // this.setState({showEditKiosk:true})
    this.setState({editKioskID: id, showEditKiosk:true})
  }

  hideEditKioskModal = (id) => {
    // this.setState({showEditKiosk:false})
    this.setState({editKioskID: null, showEditKiosk:false})
    this.loadKiosks();
  }

  componentDidReceiveProps = () => {
    this.setState({currentLevel: this.props.floorplans.length - 1});
  }

  setCurrentLevel = (level, e) => {
    this.props.setCurrentLevel(level)
    // this.setState({currentLevel: level});
  }

  zoomIn = () => {
    this.props.zoomIn();
  }
  
  zoomOut = () => {
    this.props.zoomOut();
  }

  dragging = (dragging) => {
    this.setState({dragging})
  }

  draggingKiosk = (draggingKiosk) => {
    this.setState({draggingKiosk})
  }

  place = (delta, item) => {
    // console.log(delta);
    if(item === 'Kiosk'){
      this.props.placeKiosk(delta);
      this.props.setKioskPopover(true);
    }
    if(item === 'Amenity'){
      this.props.placeAmenity(delta);
      this.props.setAmenityPopover(true);
    }
  }

  closePopOver = () => {
    this.props.setAmenityPopover(false);
    this.props.setKioskPopover(false);
  }

  editAmenityInstance = (idx) => {
    this.props.setAmenityPopoverInstance(idx)
  }

  removeAmenityInstance = (idx) => {
    this.props.removeAmenityInstance(idx);
  }

  editKioskInstance = (idx) => {
    this.props.setKioskPopoverInstance(idx)
  }

  removeKioskInstance = (idx) => {
    this.props.removeKioskInstance(idx);
    this.setState({open: true,});
  }

  hasStore = (polygonID) => {
    return this.props.assignedStores.find((assignedStore, idx) => {
      return assignedStore.polygonID === polygonID
    });
  }

  removeKiosk = (id) => {
    window.fetch(window.endpoint + '/kiosks/' + id, {method: 'DELETE'})
    .then((response) => {
      return response.text();
    })
    .then((body) => {
      const response = JSON.parse(body);
      console.log(response);
      if(response.status || response.status === false){}
      else {
        this.loadKiosks();
      }
    })
    .catch((e) => {
      console.log(e);
    })
  }
  
    render() {

      let floorPlansPolygons =[];
      let assignedStoresImage = [];
      if(this.props.floorplans.length > 0) {
        floorPlansPolygons = this.props.floorplans.map((floorplan, floorplanidx) => {
            return floorplan.floorplanPolygons.map((polygon, idx) => {

              const bb = polygon.getBBox();
              const center = {x: bb.x + bb.width / 2, y: bb.y + bb.height / 2 };

              const hasStore = this.hasStore(polygon.id);
              if(hasStore){
                const sampleStore = <div id={`assignedstores-${polygon.id}-${idx}`} key={`assignedstoreskiosk-${floorplanidx}-${polygon.id}-${idx}`} style={{position: 'absolute', left: center.x * this.props.zoom, top: center.y * this.props.zoom, backgroundSize: 'contain', backgroundPosition: 'center', width: bb.width, height: bb.height, backgroundRepeat: 'no-repeat', marginLeft: bb.width / 2 * -1, marginTop: bb.height / 2 * -1, backgroundImage: hasStore ? `url(${window.public}/${hasStore.store.store_image})` : ``, display: this.props.currentLevel === hasStore.level ? '' : 'none'}}></div>;
                
                assignedStoresImage.push(sampleStore);
              }

              const pathStyle = {
                cursor: 'pointer',
                fill: polygon.id === this.props.selectedStore || hasStore ? 'transparent' : '#78909C',
                strokeWidth: '.5',
                stroke: polygon.id === this.props.selectedStore ? '#1D89E4' : '#CCC'
              }

              return <path key={polygon.getAttribute('id')} id={polygon.getAttribute('id')} style={pathStyle} d={polygon.getAttribute('d')} />
          });
        });
      }

      let amenitiesInstances =[];
      let activeAmenityCoords = {};
      const offset = 20 / this.props.zoom;
      const offsetDialog = 50 / this.props.zoom;
      if(this.props.amenitiesInstances.length > 0) {
        amenitiesInstances = this.props.amenitiesInstances.map((amenity, amenityidx) => {
          if(this.props.amenityPopoverIdx === amenityidx) { activeAmenityCoords = {x: amenity.position.x * this.props.zoom + offsetDialog, y: amenity.position.y  * this.props.zoom + offsetDialog} }
          return <circle key={'amenity-'+amenityidx} cx={amenity.position.x + offset} cy={amenity.position.y + offset} r="3" stroke="black" strokeWidth="1" fill="transparent" style={{cursor: 'pointer', display: this.props.currentLevel === amenity.level ? '' : 'none' }} onTouchTap={this.editAmenityInstance.bind(this,amenityidx)} />
        });
      }

      let kiosksInstances = [];
      let activeKioskCoords = {};
      if(this.props.kiosksInstances.length > 0) {
        kiosksInstances = this.props.kiosksInstances.map((kiosk, kioskidx) => {
          if(this.props.kioskPopoverIdx === kioskidx) { activeKioskCoords = {x: kiosk.position.x * this.props.zoom + offsetDialog, y: kiosk.position.y  * this.props.zoom + offsetDialog} }
          return <circle key={'kiosk-'+kioskidx} cx={kiosk.position.x + offset} cy={kiosk.position.y + offset} r="3" stroke="black" strokeWidth="1" fill="transparent" style={{cursor: 'pointer', display: this.props.currentLevel === kiosk.level ? '' : 'none',stroke: "#1D89E4"  }} onTouchTap={this.editKioskInstance.bind(this,kioskidx)} />
        });
      }

        return (
        <Row style={{height: '100%'}}>
          <Col style={{padding: '20px', position: 'relative', flex: `0 0 250px`, backgroundColor: '#424242', color:'#fff', borderRight: '1px solid #ececec'}}>
            <Row>
              <Col xs={9}>
                <h2>Kiosks</h2>
                <p>Lorem ipsum sit amet</p>
              </Col>
              <Col xs={3}>
                <FloatingActionButton onTouchTap={this.showAddKioskModal} mini={true} style={{ margin: '10px 0'}}>
                  <ContentAdd />
                </FloatingActionButton>
              </Col>
            </Row>
            
            <div style={{ marginBottom: '20px'}}></div>

            <div style={{marginLeft: '-20px', marginRight:'-20px'}}>
            <Table>
            {this.state.kiosks.length > 0 &&
              <TableBody displayRowCheckbox={this.state.showCheckboxes}>
                {this.state.kiosks.map( (row, index) => (
                  <TableRow key={index}>
                    <TableRowColumn style={{width:'5px', padding: '0 20px'}}>{index + 1}</TableRowColumn>
                    <TableRowColumn>{row.kiosk_name}</TableRowColumn>
                    <TableRowColumn style={{padding: 0, width: '50px'}}>
                      <IconButton tooltip="Edit">
                        <EditStoreIcon onTouchTap={this.showEditKioskModal.bind(this, row.kiosk_id)}/>
                      </IconButton>
                    </TableRowColumn>
                    <TableRowColumn style={{padding: 0, width: '50px'}}>
                      <IconButton tooltip="Edit">
                        <RemoveStoreIcon onTouchTap={this.removeKiosk.bind(this, row.kiosk_id)}/>
                      </IconButton>
                    </TableRowColumn>
                  </TableRow>
                ))}
              </TableBody>
            }
            {this.state.kiosks.length === 0 &&
                <TableBody displayRowCheckbox={false}>
                
                <TableRow>
                    <TableRowColumn>No active kiosks found</TableRowColumn>
                  </TableRow>
                </TableBody>
              }
            </Table>
            </div>
          </Col>

          <Col xs style={{padding: '20px', position: 'relative',  background: 'linear-gradient(to bottom, rgba(255,255,255,1) 0%, rgba(246,246,246,1) 47%, rgba(237,237,237,1) 100%)'}}>
          {floorPlansPolygons.length > 0 &&
            <div style={{height: '100%'}}>
                  <div style={{height: '100%', position: 'relative'}}>
                     {floorPlansPolygons.map((polygon, idx) => {
                      const display = idx === this.props.currentLevel ? '' : 'none';
                      return (
                          <svg width="100%" height="100%" key={`floorplankiosk-${idx}`} style={{display: display, zoom: this.props.zoom, position: 'absolute', zIndex: this.state.dragging || this.state.draggingKiosk ? 4 : 6}}>
                            {amenitiesInstances}
                            {kiosksInstances}
                          </svg>
                      )
                    })}
                    {floorPlansPolygons.map((polygon, idx) => {
                      const display = idx === this.props.currentLevel ? '' : 'none';
                      return (
                          <svg width="100%" height="100%" key={`floorplan-${idx}`} style={{display: display, zoom: this.props.zoom, zIndex: 1, position: 'absolute'}}>
                            {polygon}
                          </svg>
                      )
                    })}
                    {assignedStoresImage}
                    <div style={{position: 'absolute', top: '20px', left: '20px', zIndex: 10}}>
                      <Amenity name="Amenity" dragging={this.dragging} /><span style={{display: 'inline-block', padding: '7px 0 7px 10px'}}>Drag icon to add amenity</span>
                    </div>
                    <div id="activeAmenity" style={{position: 'absolute', left: activeAmenityCoords.x, top: activeAmenityCoords.y}} ></div>

                    <div style={{position: 'absolute', top: '20px', left: '260px', zIndex: 10}}>
                      <Kiosk name="Kiosk" dragging={this.draggingKiosk} color={'#1D89E4'} /><span style={{display: 'inline-block', padding: '7px 0 7px 10px'}}>Drag icon to add kiosk</span>
                    </div>
                    <div id="activeKiosk" style={{position: 'absolute', left: activeKioskCoords.x, top: activeKioskCoords.y}} ></div>

                    <Drop place={this.place} />

                    {this.props.amenityPopover &&
                    <Popover
                      open={this.props.amenityPopover}
                      anchorEl={document.getElementById('activeAmenity')}
                      anchorOrigin={{horizontal: 'left', vertical: 'bottom'}}
                      targetOrigin={{horizontal: 'left', vertical: 'top'}}
                      onRequestClose={() => {this.closePopOver()}}
                    >
                      <Menu desktop={true} style={{margin: '0', padding: '0'}}>
                        {this.state.amenities.map( (row, index) => (
                          <MenuItem key={`popover-${index}`} primaryText={row.amenity_name} onTouchTap={() => {this.closePopOver()}}/>
                        ))}
                        <Divider />
                        <MenuItem primaryText="Remove" rightIcon={<Delete />} onTouchTap={() => { this.removeAmenityInstance(this.props.amenityPopoverIdx); }} />
                      </Menu>
                    </Popover>
                    }

                    {this.props.kioskPopover &&
                    <Popover
                      open={this.props.kioskPopover}
                      anchorEl={document.getElementById('activeKiosk')}
                      anchorOrigin={{horizontal: 'left', vertical: 'bottom'}}
                      targetOrigin={{horizontal: 'left', vertical: 'top'}}
                      onRequestClose={() => {this.closePopOver()}}
                    >
                      <Menu desktop={true} style={{margin: '0', padding: '0'}}>
                        {kioskData.map( (row, index) => (
                          <MenuItem key={`popover-${index}`} primaryText={row.kiosk_name} onTouchTap={() => {this.closePopOver()}}/>
                        ))}
                        <Divider />
                        <MenuItem primaryText="Remove" rightIcon={<Delete />} onTouchTap={() => { this.removeKioskInstance(this.props.kioskPopoverIdx); }} />
                      </Menu>
                    </Popover>
                    }

                  </div>
                    <div style={{width: '50px', position: 'absolute', right: '20px', top: '20px', zIndex: 10 }}>
                      {this.props.floorplans.map( (floorplan, idx) => {
                        return <RaisedButton onTouchTap={this.setCurrentLevel.bind(this, idx)} primary={this.props.currentLevel === idx ? true : false} key={`switchfloorplan-${idx}`} label={`${floorplan.level}`} style={{minWidth: '40px', margin: '5px'}} />
                      })}
                    </div>
                    <div style={{width: '50px', position: 'absolute', right: '20px', bottom: '20px', zIndex: 10 }}>
                      <FloatingActionButton mini={true} style={{float: 'right', margin: '10px 0'}} onTouchTap={this.zoomIn.bind(this)}>
                        <ActionZoomIn />
                      </FloatingActionButton>
                      <FloatingActionButton mini={true} style={{float: 'right', margin: '10px 0'}}  onTouchTap={this.zoomOut.bind(this)}>
                        <ActionZoomOut />
                      </FloatingActionButton>
                    </div>
                  </div>
          }
                </Col>

                <AddKiosk open={this.state.showAddKiosk} showAddKioskModal={this.showAddKioskModal} hideAddKioskModal={this.hideAddKioskModal}/>
                {/*<EditKiosk open={this.state.showEditKiosk} showEditKioskModal={this.showEditKioskModal} hideEditKioskModal={this.hideEditKioskModal} editKioskID={this.state.editKioskID}/>*/}
                <EditKiosk open={this.state.showEditKiosk} showEditKioskModal={this.showEditKioskModal} hideEditKioskModal={this.hideEditKioskModal} editKioskID={this.state.editKioskID} />

                <AddAmenity open={this.state.showAddAmenity} showAddAmenityModal={this.showAddAmenityModal} hideAddAmenityModal={this.hideAddAmenityModal}/>
                <EditAmenity open={this.state.showEditAmenity} showEditAmenityModal={this.showEditAmenityModal} hideEditAmenityModal={this.hideEditAmenityModal} editAmenityID={this.state.editAmenityID}/>
               
        </Row>
        );
    }
}


export default Kiosks;