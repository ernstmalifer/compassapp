import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { DragSource } from 'react-dnd';

import SvgIcon from 'material-ui/SvgIcon';
const AmenityIcon = (props) => (
  <SvgIcon {...props} viewBox="0 0 32 32" style={{width: '32px', height: '32px'}}>
    <g transform="matrix(1,0,0,1,0,0)">
      <path d="M16 0c-8.837 0-16 7.163-16 16s7.163 16 16 16 16-7.163 16-16-7.163-16-16-16zM16 28c-6.627 0-12-5.373-12-12s5.373-12 12-12c6.627 0 12 5.373 12 12s-5.373 12-12 12z"></path>
    </g>
  </SvgIcon>
)

import './Box.css';

const style = {
  padding: '0',
  cursor: 'move',
  float: 'left',
  animationDuration: '1s',
  animationIterationCount: 'infinite'
};

class Box extends Component {
  static propTypes = {
    connectDragSource: PropTypes.func.isRequired,
    isDragging: PropTypes.bool.isRequired,
    name: PropTypes.string.isRequired,
  };

  render() {
    const { connectDragSource } = this.props;

    return (
      connectDragSource(
        <div style={{ ...style }}>
          <AmenityIcon color={this.props.color} />
        </div>
      )
    );
  }
}

const ItemTypes = {
  BOX: 'box',
};

const boxSource = {
  beginDrag(props) {
    props.dragging(true);
    return {
      name: props.name,
    };
  },

  endDrag(props, monitor) {
    props.dragging(false);
    // const item = monitor.getItem();
    const dropResult = monitor.getDropResult();

    if (dropResult) {
    }
  },
};

function collect(connect, monitor) {
  return {
    connectDragSource: connect.dragSource(),
    isDragging: monitor.isDragging()
  }
}

export default DragSource(ItemTypes.BOX, boxSource, collect)(Box);