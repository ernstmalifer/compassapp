import React, { Component } from 'react';
// import PropTypes from 'prop-types';
import { DropTarget } from 'react-dnd';

const squareTarget = {
  drop(props, monitor, component) {
    const item = monitor.getInitialClientOffset();
    const delta = monitor.getDifferenceFromInitialOffset();
    const left = Math.round(item.x + delta.x - 635); // Magic Number
    const top = Math.round(item.y + delta.y - 150);

    // props.place(delta, monitor.getItem().name);
    props.place({x: left, y: top}, monitor.getItem().name);
  }
};

function collect(connect, monitor) {
  return {
    connectDropTarget: connect.dropTarget(),
    isOver: monitor.isOver()
  };
}

class Drop extends Component {
  render() {
    const { connectDropTarget, isOver } = this.props;
    // const black = (x + y) % 2 === 1;

    return connectDropTarget(
      <div className="droppable" style={{
        width: '100%',
        height: '100%',
        position: 'absolute',
        top: 0,
        left: 0,
        zIndex: 5,
        backgroundColor: isOver ? '' : ''
      }}>
      </div>
    );
  }
}

const ItemTypes = {
  BOX: 'box',
};

export default DropTarget(ItemTypes.BOX, squareTarget, collect)(Drop);