import React from 'react';

import update from 'immutability-helper';

import {
  Route
} from 'react-router-dom';

import FloorPlanManagement from './FloorPlanManagement';
import AssignedStores from './AssignedStores';
import Amenities from './Amenities';
import Kiosks from './Kiosks';
import Events from './Events';

import {Card, CardText} from 'material-ui/Card';


import 'whatwg-fetch';
import samplemap from '../../../public/map_sample.svg';

class FloorPlan extends React.Component {

  constructor(props){
    super(props);

    this.state = {
      floorplans: [],
      tempFloorPlan: {},
      zoom: 1.0,
      currentLevel: 0,
      amenitiesInstances: [],
      amenityPopover: false,
      amenityPopoverIdx: null,
      kiosksInstances: [],
      kioskPopover: false,
      kioskPopoverIdx: null,
      selectedStore: null,
      selectedStorePopover: false,
      assignedStores: [],
      dev: false
    }
  }

  componentDidMount(){

    if(this.state.dev){
      // Load a sample floorplan
      this.addTempFloorPlanForTest();
    }

    this.loadFloorplans();

  }

  addTempFloorPlanForTest = () => {

    window.fetch(samplemap)
    .then((response) => {
      return response.text()
    }).then((body) => {
      
      var parser = new DOMParser();

      const xmlData = parser.parseFromString(body, "application/xml");
      const polygons = xmlData.getElementsByTagName('path');

      let tempFloorPlan = {  floorplanPolygons: [] };
      for (let i = 0; i < polygons.length; i++) {
        tempFloorPlan.floorplanPolygons.push(polygons[i]);
      }

      let saveTempFloorPlan = Object.assign({}, tempFloorPlan);
      saveTempFloorPlan.name = 'Ground Level';
      saveTempFloorPlan.level = '1';

      this.appendLocalFloorplan(saveTempFloorPlan);

    })

  }

  setTempFloorPlan = (tempFloorPlan) => {
    this.setState({tempFloorPlan});
  }

  loadFloorplans = () => {
    this.setState({floorplans: []});
    window.fetch(window.endpoint + '/floorplans')
    .then((response) => {
      return response.text();
    })
    .then((body) => {
      const response = JSON.parse(body);
      if(response.status || response.status === false){}
      else {

          response.floorplans.forEach((floorplan)=> {

            if(floorplan.floorplan_file){
              window.fetch(`${window.public}/${floorplan.floorplan_file}`)
              .then((response) => {
                return response.text()
              }).then((body) => {
                
                var parser = new DOMParser();

                const xmlData = parser.parseFromString(body, "application/xml");
                const polygons = xmlData.getElementsByTagName('path');

                let tempFloorPlan = {  floorplanPolygons: [] };
                for (let i = 0; i < polygons.length; i++) {
                  tempFloorPlan.floorplanPolygons.push(polygons[i]);
                }

                let saveTempFloorPlan = Object.assign({}, tempFloorPlan);
                saveTempFloorPlan.floorplan_id = floorplan.floorplan_id;
                saveTempFloorPlan.name = floorplan.floorplan_name;
                saveTempFloorPlan.level = floorplan.floorplan_level;

                this.appendLocalFloorplan(saveTempFloorPlan);

              })
            }

          });

          this.loadAssignedStores();
          
      }
    })
    .catch((e) => {
      console.log(e);
    })
  }

  appendLocalFloorplan = (tempFloorPlan) => {
    const toupdate = update(this.state, { floorplans: { $push: [tempFloorPlan]}});
    this.setState(toupdate, ()=>{
      if(this.state.floorplans.length === 1) {

        this.setState({currentLevel: 0});
      }
    });
  }

  saveTempFloorPlan = (saveTempFloorPlan) => {
    
    // console.log(window.tempFloorPlanFile);
    // const toupdate = update(this.state, { floorplans: { $push: [saveTempFloorPlan]}});
    // this.setState(toupdate, ()=>{
    //   if(this.state.floorplans.length === 1) {

    //     this.setState({currentLevel: 0});
    //   }
    // });
    // this.setState({tempFloorPlan: {}});

    // console.log(saveTempFloorPlan);

    // const floorplan = {
    //   floorplan_name: saveTempFloorPlan.name,
    //   floorplan_level: saveTempFloorPlan.level,
    //   is_active: '1'
    // }

    // var data = new FormData()
    // data.append('data', JSON.stringify(floorplan));

    // if(window.tempFloorPlanFile){
    //   data.append('file', window.tempFloorPlanFile);
    // }
    // window.fetch(window.endpoint + '/floorplans', {
    //   method: 'POST',
    //   body: data
    // })
    // .then((response) => {
    //   return response.text();
    // })
    // .then((body) => {
    //   window.tempFloorPlanFile = null;
    //   this.loadFloorplans();
    // })
    // .catch((error) => {
    //   console.log(error);
    // })
    this.loadFloorplans();
    
  }

  updateFloorPlan = (tofloorplan) => {

    // const floorplan = {
    //   floorplan_name: tofloorplan.name,
    //   floorplan_level: tofloorplan.level,
    //   is_active: '1'
    // }

    // var data = new FormData()
    // data.append('data', JSON.stringify(floorplan));

    // // if(window.tempFloorPlanFile){
    // //   data.append('file', window.tempFloorPlanFile);
    // // }
    // window.fetch(window.endpoint + '/floorplans/' + tofloorplan.floorplan_id , {
    //   method: 'POST',
    //   body: data
    // })
    // .then((response) => {
    //   return response.text();
    // })
    // .then((body) => {
    //   window.tempFloorPlanFile = null;
    //   this.loadFloorplans();
    // })
    // .catch((error) => {
    //   console.log(error);
    // })

    this.loadFloorplans();

  }

  removeFloorPlan = (id) => {

    // const toupdate = update(this.state, { floorplans: { $splice: [[idx,1]]}});
    // this.setState(toupdate);
    window.fetch(window.endpoint + '/floorplans/' + id, {method: 'DELETE'})
    .then((response) => {
      return response.text();
    })
    .then((body) => {
      const response = JSON.parse(body);
      if(response.status || response.status === false){}
      else {
        this.loadFloorplans();
      }
    })
    .catch((e) => {
      console.log(e);
    })
  }

  zoomIn = () => {
    this.setState({zoom: this.state.zoom + 0.1})
  }
  
  zoomOut = () => {
    if(this.state.zoom >= 1){
      this.setState({zoom: this.state.zoom - 0.1})
    }
  }

  setCurrentLevel = (currentLevel) => {
    this.setState({currentLevel});
  }

  placeAmenity = (delta) => {
    const offset = 15 / this.state.zoom;
    const amenity = { level: this.state.currentLevel, position: { x: delta.x / this.state.zoom + offset, y: delta.y / this.state.zoom + offset }};
    const toupdate = update(this.state, { amenitiesInstances: { $push: [amenity]}});
    this.setState(toupdate, ()=> {
      this.setState({amenityPopover: true, amenityPopoverIdx: this.state.amenitiesInstances.length - 1});
    });
  }

  setAmenityPopover = (popover) => {
    this.setState({amenityPopover: popover});
  }

  setAmenityPopoverInstance = (idx) => {
    this.setState({amenityPopover: true, amenityPopoverIdx: idx});
  }

  removeAmenityInstance = (idx) => {
    const toupdate = update(this.state, { amenitiesInstances: { $splice: [[idx,1]]}});
    this.setState(toupdate,  ()=> {
      this.setState({amenityPopover: false});
    });
  }

  placeKiosk = (delta) => {
    const offset = 15 / this.state.zoom;
    const kiosk = { level: this.state.currentLevel, position: { x: delta.x / this.state.zoom + offset, y: delta.y / this.state.zoom + offset }};
    const toupdate = update(this.state, { kiosksInstances: { $push: [kiosk]}});
    this.setState(toupdate, ()=> {
      this.setState({kioskPopover: true, kioskPopoverIdx: this.state.kiosksInstances.length - 1});
    });
  }

  setKioskPopover = (popover) => {
    this.setState({kioskPopover: popover});
  }

  setKioskPopoverInstance = (idx) => {
    this.setState({kioskPopover: true, kioskPopoverIdx: idx});
  }

  removeKioskInstance = (idx) => {
    const toupdate = update(this.state, { kiosksInstances: { $splice: [[idx,1]]}});
    this.setState(toupdate,  ()=> {
      this.setState({kioskPopover: false});
    });
  }

  setSelectedStore = (id) => {
    this.setState({selectedStore: id}, () => {
      this.setState({selectedStorePopover: true});
    });
  }

  setSelectedStorePopover = (popover) => {
    this.setState({selectedStorePopover: popover});
  }

  loadAssignedStores = () => {


    window.fetch(window.endpoint + '/storeassignments')
    .then((response) => {
      return response.text();
    })
    .then((body) => {
      const response = JSON.parse(body);
      if(response.status || response.status === false){ this.setState({assignedStores: []}); }
      else {
        this.setState({assignedStores: []});
        response.storeassignments.forEach((assignedstore) => {
          this.setAssignedStoreToPolygon(assignedstore.room_id, assignedstore.room_name, assignedstore.store, assignedstore.floor_plan_id);
        });
      }
    })
    .catch((e) => {
      console.log(e);
    })
  }

  setAssignedStore = (polygonID, store, id) => {

    let floorplan_id = '';
    this.state.floorplans.forEach((floorplan, idx) => {
      if(idx === this.state.currentLevel){
        floorplan_id = floorplan.floorplan_id;
      }
    });

    const storeassignment = {
      floor_plan_id: floorplan_id,
      store_id: store.id,
      room_name: polygonID,
      is_active: 1
    }

    var data = new FormData()
    data.append('data', JSON.stringify(storeassignment));

    window.fetch(`${window.endpoint}/storeassignments${id ? '/' + id : ''}`, {
      method: 'POST',
      body: data
    })
    .then((response) => {
      return response.text();
    })
    .then((body) => {
      this.loadAssignedStores();
    })
    .catch((error) => {
      console.log(error);
    })

  }

  setAssignedStoreFromPanel = (polygonID, store, floorplan_id, id) => {

    const storeassignment = {
      floor_plan_id: floorplan_id,
      store_id: store,
      room_name: polygonID,
      is_active: 1
    }

    var data = new FormData()
    data.append('data', JSON.stringify(storeassignment));

    window.fetch(`${window.endpoint}/storeassignments${id ? '/' + id : ''}`, {
      method: 'POST',
      body: data
    })
    .then((response) => {
      return response.text();
    })
    .then((body) => {
      this.loadAssignedStores();
    })
    .catch((error) => {
      console.log(error);
    })

  }

  setAssignedStoreToPolygon = (id, polygonID, store, floor_plan_id) => {
    
    // const index = this.state.assignedStores.map((assignedStore, idx) => {
    //   return assignedStore.polygonID;
    // }).indexOf(polygonID);

    let level = this.state.currentLevel;
    let levelName;
    if(floor_plan_id){
      this.state.floorplans.forEach((floorplan, idx) => {
        if(floorplan.floorplan_id === floor_plan_id){
          level = idx;
          levelName = floorplan.level;
        }
      });
    }

    // console.log(index);

    // if(index !== -1){
    //   const toupdate = update(this.state, { assignedStores: { $splice: [[index, 1, {polygonID: polygonID, level: level, store: store}]] }});
    //   this.setState(toupdate, ()=> {});
    // } else {
      const toupdate = update(this.state, { assignedStores: { $push: [{id: id, polygonID: polygonID,  level: level, store: store, levelName: levelName}]}});
      this.setState(toupdate, ()=> {});
    // }

  }

  removeAssignedStore = (assignedstoreId) => {
    window.fetch(`${window.endpoint}/storeassignments/${assignedstoreId}`, {
      method: 'DELETE'
    })
    .then((response) => {
      return response.text();
    })
    .then((body) => {
      this.loadAssignedStores();
    })
    .catch((error) => {
      console.log(error);
    })
  }

  render() {

    return (
      <Card className="MainCard" containerStyle={{ paddingBottom: 0, height: 'inherit' }}>
        
        <CardText style={{height: 'inherit', position: 'relative', padding: '0px'}}>
        
        <Route path={`${this.props.match.url}/`} render={(props) => <FloorPlanManagement saveTempFloorPlan={this.saveTempFloorPlan}  setTempFloorPlan={this.setTempFloorPlan} floorplans={this.state.floorplans} zoom={this.state.zoom} zoomIn={this.zoomIn} zoomOut={this.zoomOut} currentLevel={this.state.currentLevel} setCurrentLevel={this.setCurrentLevel} tempFloorPlan={this.state.tempFloorPlan} removeFloorPlan={this.removeFloorPlan} {...props} />}  />
        
        <Route path={`${this.props.match.url}/assignedstores`} render={(props) => <AssignedStores  floorplans={this.state.floorplans} zoom={this.state.zoom} zoomIn={this.zoomIn} zoomOut={this.zoomOut} currentLevel={this.state.currentLevel} setCurrentLevel={this.setCurrentLevel} selectedStore={this.state.selectedStore} setSelectedStore={this.setSelectedStore} selectedStorePopover={this.state.selectedStorePopover} setSelectedStorePopover={this.setSelectedStorePopover} assignedStores={this.state.assignedStores} setAssignedStore={this.setAssignedStore} setAssignedStoreFromPanel={this.setAssignedStoreFromPanel} removeAssignedStore={this.removeAssignedStore} />} />
        
        <Route path={`${this.props.match.url}/amenities`} render={(props) => <Amenities saveTempFloorPlan={this.saveTempFloorPlan}  setTempFloorPlan={this.setTempFloorPlan} floorplans={this.state.floorplans} zoom={this.state.zoom} zoomIn={this.zoomIn} zoomOut={this.zoomOut} currentLevel={this.state.currentLevel} setCurrentLevel={this.setCurrentLevel} tempFloorPlan={this.state.tempFloorPlan} amenitiesInstances={this.state.amenitiesInstances} amenityPopover={this.state.amenityPopover} placeAmenity={this.placeAmenity} setAmenityPopover={this.setAmenityPopover} amenityPopoverIdx={this.state.amenityPopoverIdx} setAmenityPopoverInstance={this.setAmenityPopoverInstance} removeAmenityInstance={this.removeAmenityInstance} kiosksInstances={this.state.kiosksInstances} kioskPopover={this.state.kioskPopover} placeKiosk={this.placeKiosk} setKioskPopover={this.setKioskPopover} kioskPopoverIdx={this.state.kioskPopoverIdx} setKioskPopoverInstance={this.setKioskPopoverInstance} removeKioskInstance={this.removeKioskInstance} assignedStores={this.state.assignedStores} {...props} />}/>
        
        <Route path={`${this.props.match.url}/kiosks`} render={(props) => <Kiosks saveTempFloorPlan={this.saveTempFloorPlan}  setTempFloorPlan={this.setTempFloorPlan} floorplans={this.state.floorplans} zoom={this.state.zoom} zoomIn={this.zoomIn} zoomOut={this.zoomOut} currentLevel={this.state.currentLevel} setCurrentLevel={this.setCurrentLevel} tempFloorPlan={this.state.tempFloorPlan} amenitiesInstances={this.state.amenitiesInstances} amenityPopover={this.state.amenityPopover} placeAmenity={this.placeAmenity} setAmenityPopover={this.setAmenityPopover} amenityPopoverIdx={this.state.amenityPopoverIdx} setAmenityPopoverInstance={this.setAmenityPopoverInstance} removeAmenityInstance={this.removeAmenityInstance} kiosksInstances={this.state.kiosksInstances} kioskPopover={this.state.kioskPopover} placeKiosk={this.placeKiosk} setKioskPopover={this.setKioskPopover} kioskPopoverIdx={this.state.kioskPopoverIdx} setKioskPopoverInstance={this.setKioskPopoverInstance} removeKioskInstance={this.removeKioskInstance} assignedStores={this.state.assignedStores} {...props} />} />
        
        <Route path={`${this.props.match.url}/events`} component={Events} />
      </CardText>
        
      </Card>
    );
  }
}

// export default DragDropContext(HTML5Backend)(FloorPlan);
export default FloorPlan;