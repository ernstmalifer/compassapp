import React from 'react';
import { Row, Col } from 'react-flexbox-grid';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import ContentAdd from 'material-ui/svg-icons/content/add';
import ActionZoomIn from 'material-ui/svg-icons/action/zoom-in';
import ActionZoomOut from 'material-ui/svg-icons/action/zoom-out';
import RaisedButton from 'material-ui/RaisedButton';
import Delete from 'material-ui/svg-icons/action/delete';
import {
    Table,
    TableBody,
    TableRow,
    TableRowColumn,
} from 'material-ui/Table';
import Popover from 'material-ui/Popover';
import Divider from 'material-ui/Divider';
import Menu from 'material-ui/Menu';
import MenuItem from 'material-ui/MenuItem';

import SvgIcon from 'material-ui/SvgIcon';
import IconButton from 'material-ui/IconButton';

import AssignStore from './Modules/AssignStore.js';
import EditAssignedStore from './Modules/EditAssignedStore.js';


const EditStoreIcon = (props) => (
  <SvgIcon {...props}>
    <g transform="matrix(.75,0,0,.75,2,0)">
    <path d="M10.681 18.207l-2.209 5.67 5.572-2.307-3.363-3.363zM26.855 6.097l-0.707-0.707c-0.78-0.781-2.047-0.781-2.828 0l-1.414 1.414 3.535 3.536 1.414-1.414c0.782-0.781 0.782-2.048 0-2.829zM10.793 17.918l0.506-0.506 3.535 3.535 9.9-9.9-3.535-3.535 0.707-0.708-11.113 11.114zM23.004 26.004l-17.026 0.006 0.003-17.026 11.921-0.004 1.868-1.98h-14.805c-0.552 0-1 0.447-1 1v19c0 0.553 0.448 1 1 1h19c0.553 0 1-0.447 1-1v-14.058l-2.015 1.977 0.054 11.085z"></path>
    </g>
  </SvgIcon>
)

const RemoveStoreIcon = (props) => (
  <SvgIcon {...props}>
    <g transform="matrix(.6,0,0,.6,3,3)">
    <path d="M4 10v20c0 1.1 0.9 2 2 2h18c1.1 0 2-0.9 2-2v-20h-22zM10 28h-2v-14h2v14zM14 28h-2v-14h2v14zM18 28h-2v-14h2v14zM22 28h-2v-14h2v14z"></path>
    <path d="M26.5 4h-6.5v-2.5c0-0.825-0.675-1.5-1.5-1.5h-7c-0.825 0-1.5 0.675-1.5 1.5v2.5h-6.5c-0.825 0-1.5 0.675-1.5 1.5v2.5h26v-2.5c0-0.825-0.675-1.5-1.5-1.5zM18 4h-6v-1.975h6v1.975z"></path>
    </g>
  </SvgIcon>
)

// const tableData = [
//   {store_name: 'Nike', store_image: 'https://upload.wikimedia.org/wikipedia/commons/thumb/a/a6/Logo_NIKE.svg/1200px-Logo_NIKE.svg.png'}, 
//   {store_name: 'Adidas', store_image: 'https://upload.wikimedia.org/wikipedia/commons/thumb/2/20/Adidas_Logo.svg/2000px-Adidas_Logo.svg.png'},
//   {store_name: 'Underarmour', store_image: 'https://upload.wikimedia.org/wikipedia/commons/4/44/Under_armour_logo.svg'},
//   {store_name: 'Reebok', store_image: 'http://www.pngmart.com/files/4/Reebok-Logo-PNG-File.png'},
//   {store_name: 'Puma', store_image: 'https://upload.wikimedia.org/wikipedia/en/4/49/Puma_AG.svg'},
//   {store_name: 'Columbia', store_image: 'https://upload.wikimedia.org/wikipedia/en/c/c2/Columbia_Sportswear_Co_logo.svg'},
//   {store_name: 'New Balance', store_image: 'https://upload.wikimedia.org/wikipedia/commons/thumb/e/ea/New_Balance_logo.svg/640px-New_Balance_logo.svg.png'},
//   {store_name: 'Oakley', store_image: 'https://upload.wikimedia.org/wikipedia/commons/thumb/8/88/Oakley_logo.svg/500px-Oakley_logo.svg.png'},
//   {store_name: 'Vans', store_image: 'https://upload.wikimedia.org/wikipedia/commons/thumb/9/91/Vans-logo.svg/500px-Vans-logo.svg.png'},
//   {store_name: 'Converse', store_image: 'https://vignette4.wikia.nocookie.net/logopedia/images/5/58/Converse_logo_black.png/revision/latest/scale-to-width-down/640?cb=20150724022851'},
//   {store_name: 'The North Face', store_image: 'https://upload.wikimedia.org/wikipedia/en/e/e9/The_North_Face_logo.svg'}
// ];


export default class AssignedStores extends React.Component {
  
  state = {
    fixedHeader: true,
    fixedFooter: true,
    stripedRows: false,
    showRowHover: false,
    selectable: true,
    multiSelectable: false,
    enableSelectAll: false,
    deselectOnClickaway: true,
    showCheckboxes: false,
    floorplans: this.props.floorplans,
    tempFloorPlan: this.props.tempFloorPlan,
    currentLevel: this.props.currentLevel,
    zoom: this.props.zoom,
    selectedStore: this.props.selectedStore,
    open: false,
    showAssignStore: false,
    showEditAssignedStore: false,
    stores: [],
    storesLimit: 100,
    storesOffset: 0,
    editAssignedStoreId: null
  };

  componentWillMount = () => {
    this.loadStores();
  }

  loadStores = () => {
    window.fetch(`${window.endpoint}/stores?limit=${this.state.storesLimit}&skip=${this.state.storesOffset}`).then((response) => {
      return response.text();
    })
    .then((body) => {
      const response = JSON.parse(body);
      if(response.status || response.status === false){}
      else {
        this.setState({stores: response.stores});
      }
    })
    .catch((e) => {
      console.log(e);
    })
  }

  showAssignStoreModal = (event) => {
    this.setState({showAssignStore:true})
  }

  hideAssignStoreModal = (event) => {
    this.setState({showAssignStore:false})
  }

  showEditAssignedStoreModal = (id) => {
    this.setState({editAssignedStoreId: id}, ()=> {
      this.setState({showEditAssignedStore:true});
    })
  }

  hideEditAssignedStoreModal = (event) => {
    this.setState({showEditAssignedStore:false, editAssignedStoreId: null})
  }

  closePopOver = () => {
    this.props.setSelectedStorePopover(false);
  }

  setCurrentLevel = (level, e) => {
    this.props.setCurrentLevel(level)
    // this.setState({currentLevel: level});
  }

  zoomIn = () => {
    this.props.zoomIn();
  }

  zoomOut = () => {
    this.props.zoomOut();
  }

  setSelectedStore = (id) => {
    this.props.setSelectedStore(id);
  }

  setAssignedStore = (store) => {

    const already = this.props.assignedStores.find((assignedstore) => {
      return this.props.selectedStore === assignedstore.polygonID;
    })

    if(already){
      this.props.setAssignedStore(this.props.selectedStore, store, already.id);
    } else {
      this.props.setAssignedStore(this.props.selectedStore, store);
    }

    this.closePopOver();
  }

  setAssignedStoreFromPanel = (polygonID, store, floorplan_id, id) => {
    this.props.setAssignedStoreFromPanel(polygonID, store, floorplan_id, id);
  }

  hasStore = (polygonID) => {
    return this.props.assignedStores.find((assignedStore, idx) => {
      return assignedStore.polygonID === polygonID
    });
  }

  removeAssignedStore = (assignedstore) => {
    this.props.removeAssignedStore(assignedstore.id)
    this.closePopOver();
  }

  render() {

    let floorPlansPolygons =[];
    let activePolygon = {};
    let assignedStoresImage = [];
    if(this.props.floorplans.length > 0) {
      floorPlansPolygons = this.props.floorplans.map((floorplan, floorplanidx) => {
          return floorplan.floorplanPolygons.map((polygon, idx) => {

            const bb = polygon.getBBox();
            const center = {x: bb.x + bb.width / 2, y: bb.y + bb.height / 2 };

            if(this.props.selectedStore === polygon.id) {
              activePolygon = Object.assign({}, center);
            }

            const hasStore = this.hasStore(polygon.id);
            if(hasStore){
              const sampleStore = <div id={`assignedstores-${polygon.id}-${idx}`} key={`assignedstores-${floorplanidx}-${polygon.id}-${idx}`} style={{position: 'absolute', left: center.x * this.props.zoom, top: center.y * this.props.zoom, backgroundSize: 'contain', backgroundPosition: 'center', width: bb.width, height: bb.height, backgroundRepeat: 'no-repeat', marginLeft: bb.width / 2 * -1, marginTop: bb.height / 2 * -1, backgroundImage: hasStore ? `url(${window.public}/${hasStore.store.store_image})` : ``, display: this.props.currentLevel === hasStore.level ? '' : 'none', zIndex: 0}}></div>;
              
              assignedStoresImage.push(sampleStore);
            }

            const pathStyle = {
              cursor: 'pointer',
              fill: polygon.id === this.props.selectedStore || hasStore ? 'transparent' : '#78909C',
              strokeWidth: '.5',
              stroke: polygon.id === this.props.selectedStore ? '#1D89E4' : '#CCC'
            }
            return <path key={polygon.getAttribute('id')} id={polygon.getAttribute('id')} style={pathStyle} d={polygon.getAttribute('d')} onClick={this.setSelectedStore.bind(this, polygon.id)} />
        });
      });
    }

    const selectedHasStore = this.hasStore(this.props.selectedStore)

    return (
      <Row style={{height: '100%', background:'#363636'}}>
          <Col style={{padding: '20px', position: 'relative', flex: `0 0 250px`, backgroundColor: '#424242', color:'#fff', borderRight: '1px solid #ececec'}}>
            <Row>
              <Col xs={9}>
                <h2>Assigned Stores</h2>
                <p>Lorem ipsum sit amet</p>
              </Col>
              <Col xs={3}>
                <FloatingActionButton mini={true} style={{ margin: '10px 0'}}>
                  <ContentAdd onTouchTap={this.showAssignStoreModal}/>
                </FloatingActionButton>
                {/*<FloatingActionButton mini={true} style={{ margin: '10px 0'}}>
                  <ContentAdd />
                </FloatingActionButton>*/}
              </Col>
            </Row>
            
            <div style={{ marginBottom: '20px'}}></div>

            <div style={{marginLeft: '-20px', marginRight:'-20px'}}>
            <Table height={'500px'}>
              <TableBody displayRowCheckbox={this.state.showCheckboxes}>

                {this.props.assignedStores.map( (row, index) => (
                  <TableRow key={index}>
                    {/*<TableRowColumn style={{width:'5px', padding: '0 20px'}}>{index + 1}</TableRowColumn>*/}
                    <TableRowColumn>
                      {row.levelName &&
                        <span>
                          {row.store.store_name} - ({row.levelName}){row.polygonID}
                        </span>
                      }
                      {!row.levelName &&
                        <span style={{textDecoration: 'line-through'}}>
                          {row.store.store_name} - ({row.levelName}){row.polygonID}
                        </span>
                      }
                    </TableRowColumn>
                    <TableRowColumn style={{padding: 0, width: '50px'}}>
                      <IconButton tooltip="Edit" onTouchTap={this.showEditAssignedStoreModal.bind(this,row.id)}>
                        <EditStoreIcon />
                      </IconButton>
                    </TableRowColumn>
                    <TableRowColumn style={{padding: 0, width: '50px'}}>
                      <IconButton tooltip="Edit">
                        <RemoveStoreIcon onTouchTap={this.removeAssignedStore.bind(this, row)}/>
                      </IconButton>
                    </TableRowColumn>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
            </div>
          </Col>

          <Col xs style={{padding: '20px', position: 'relative',  background: 'linear-gradient(to bottom, rgba(255,255,255,1) 0%, rgba(246,246,246,1) 47%, rgba(237,237,237,1) 100%)'}}>
              {floorPlansPolygons.length > 0 &&
            <div style={{height: '100%'}}>
                <div style={{height: '100%', position: 'relative'}}>
                  {floorPlansPolygons.map((polygon, idx) => {
                    const display = idx === this.props.currentLevel ? '' : 'none';
                    return (
                        <svg width="100%" height="100%" key={`floorplan-${idx}`} style={{display: display, zoom: this.props.zoom, position: 'absolute', zIndex: 1}}>
                          {polygon}
                        </svg>
                    )
                  })}
                  {assignedStoresImage}

                  <div id="activePolygon" style={{position: 'absolute', left: activePolygon.x ? activePolygon.x * this.props.zoom : 0, top: activePolygon.y ? activePolygon.y * this.props.zoom : 0}} ></div>

                  <Popover
                    open={this.props.selectedStorePopover}
                    anchorEl={document.getElementById('activePolygon')}
                    anchorOrigin={{horizontal: 'left', vertical: 'bottom'}}
                    targetOrigin={{horizontal: 'left', vertical: 'top'}}
                    onRequestClose={() => {this.closePopOver()}}
                  >
                    <Menu desktop={true} style={{margin: '0', padding: '0'}} width={300} maxHeight={500}>
                      {this.state.stores.map( (store, index) => (
                        <MenuItem checked={selectedHasStore && selectedHasStore.store.id === store.id} key={`popover-${index}`} primaryText={store.store_name} onTouchTap={() => {this.setAssignedStore(store)}}/>
                      ))}
                      {selectedHasStore &&
                          <Divider />
                        }
                      {selectedHasStore &&
                          <MenuItem primaryText="Remove" rightIcon={<Delete />} onTouchTap={() => { this.removeAssignedStore(selectedHasStore); }} />
                      }
                    </Menu>
                  </Popover>

                </div>
                <div style={{width: '50px', position: 'absolute', right: '20px', top: '20px', zIndex: 10 }}>
                  {this.props.floorplans.map( (floorplan, idx) => {
                    return <RaisedButton onTouchTap={this.setCurrentLevel.bind(this, idx)} primary={this.props.currentLevel === idx ? true : false} key={`switchfloorplan-${idx}`} label={`${floorplan.level}`} style={{minWidth: '40px', margin: '5px'}} />
                  })}
                </div>
                <div style={{width: '50px', position: 'absolute', right: '20px', bottom: '20px', zIndex: 10 }}>
                  <FloatingActionButton mini={true} style={{float: 'right', margin: '10px 0'}} onTouchTap={this.zoomIn.bind(this)}>
                    <ActionZoomIn />
                  </FloatingActionButton>
                  <FloatingActionButton mini={true} style={{float: 'right', margin: '10px 0'}}  onTouchTap={this.zoomOut.bind(this)}>
                    <ActionZoomOut />
                  </FloatingActionButton>
                </div>
                </div>
                }
          </Col>

          <AssignStore open={this.state.showAssignStore} stores={this.state.stores} hideAssignStoreModal={this.hideAssignStoreModal} floorplans={this.props.floorplans} setAssignedStoreFromPanel={this.setAssignedStoreFromPanel}/>
          <EditAssignedStore open={this.state.showEditAssignedStore} floorplans={this.props.floorplans} hideEditAssignedStoreModal={this.hideEditAssignedStoreModal} editAssignedStoreId={this.state.editAssignedStoreId} setAssignedStoreFromPanel={this.setAssignedStoreFromPanel}/>

        </Row>
    );
  }
}