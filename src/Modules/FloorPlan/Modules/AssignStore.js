import React from 'react';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';

import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';

import { Row, Col } from 'react-flexbox-grid';

const customContentStyle = {
  width: '25%',
  maxWidth: 'none',
};


export default class AssignStore extends React.Component {
  state = {
    saving: false,
    open: this.props.open,
    storeId: null,
    storeFloorplanId: null,
    value: 2,
  };

  assignedstore = {};

  handleChange = (event, index, value) => this.setState({value});

  handleOpen = () => {
    this.setState({open: true});
  };

  handleClose = () => {
    this.setState({open: false, saving: false});
    this.props.hideAssignStoreModal();
  };

  componentWillReceiveProps = (nextProps) => {
    if(nextProps.open){
      this.setState({open: nextProps.open, storeId: null, storeFloorplanId: null});
    }
  }

  handleStoreChange = (event, index, value) => this.setState({storeId: value});
  handleFloorplanChange = (event, index, value) => this.setState({storeFloorplanId: value});

  addStoreAssignment = () => {

    if(!this.state.saving){
      this.setState({saving: true}, ()=> {
        this.props.setAssignedStoreFromPanel(this.assignedstore.polygonID.getValue(), this.state.storeId, this.state.storeFloorplanId);
        this.handleClose();
      });
    }

  }

  render() {
    const actions = [
      <FlatButton
        label="Cancel"
        primary={true}
        onTouchTap={this.handleClose}
        style={{color: '#CCC', marginRight: '10px' }}
        hoverColor="white"
      />,
      <RaisedButton disabled={this.state.saving} label="Add" onTouchTap={this.addStoreAssignment} backgroundColor="#1D89E4" labelColor="#FFF"/>
    ];

    return (
      <div>
        <Dialog title="Assign Store"  
          actions={actions}
          modal={true}
          className="assignStoreDialog"
          open={this.state.open}
          onRequestClose={this.handleClose}
          autoScrollBodyContent={true}
          autoDetectWindowHeight={true}
          repositionOnUpdate={true}
          contentStyle={customContentStyle}
        >

        <Row>
          <Col xs>
              <SelectField floatingLabelText="Store" value={this.state.storeId} fullWidth={true} onChange={this.handleStoreChange}>
                  {this.props.stores.map((store, idx)=> {
                  return <MenuItem key={`store-${store.id}`} value={store.id} primaryText={store.store_name} />
                })}
              </SelectField>
          </Col>
        </Row>

        <Row>
          <Col xs>
              <SelectField floatingLabelText="Floorplan" value={this.state.storeFloorplanId} fullWidth={true} onChange={this.handleFloorplanChange}>
                  {this.props.floorplans.map((floorplan, idx)=> {
                  return <MenuItem key={`floorplan-${floorplan.floorplan_id}`} value={floorplan.floorplan_id} primaryText={floorplan.level} />
                })}
              </SelectField>
          </Col>
        </Row>

        <Row>
            <Col xs>
                <TextField floatingLabelText="Room Number" floatingLabelFixed={true} fullWidth={true} defaultValue={''} ref={(input) => this.assignedstore.polygonID = input} />
            </Col>
        </Row>            
        
        </Dialog>
      </div>
    );
  }
}
