import React from 'react';


import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';

import Dropzone from 'react-dropzone';

import { Row, Col } from 'react-flexbox-grid';


export default class EditAmenity extends React.Component {
  state = {
    saving: false,
    loaded: false,
    open: this.props.open,
    amenity: {},
    imageFiles: [],
  };

  amenity = {

  };

  handleOpen = () => {
    this.setState({open: true});
  };

  handleClose = () => {
    this.setState({open: false, imageFiles: []});
    this.props.hideEditAmenityModal();
  };

  onDrop = (imageFiles) => {
    this.setState({
        imageFiles: imageFiles
    })
    console.log(imageFiles)  
    }

  componentWillReceiveProps = (nextProps) => {
      this.setState({open: nextProps.open, loaded: false});
      if(this.props.editAmenityID !== nextProps.editAmenityID && nextProps.editAmenityID !== null){
          window.fetch(window.endpoint + '/amenities/' + nextProps.editAmenityID)
          .then((response) => {
            return response.text();
          })
          .then((body) => {
            const response = JSON.parse(body);
            if(response.status || response.status === false){}
            else {
              this.setState({loaded: true, amenity: response});
            }
          })
          .catch((e) => {
            console.log(e);
          })
        }
      
  }

  updateAmenity = () => {

    this.setState({saving: true});

    const amenity = {
      amenity_name: this.amenity.amenity_name.getValue(),
      amenity_description: this.amenity.amenity_description.getValue(),
      is_active: '1'
    }

    var data = new FormData()
    data.append('data', JSON.stringify(amenity));

    if(this.state.imageFiles.length > 0){
      data.append('file', this.state.imageFiles[0]);
    }
    window.fetch(window.endpoint + '/amenities/' + this.props.editAmenityID, {
      method: 'POST',
      body: data
    })
    .then((response) => {
      return response.text();
    })
    .then((body) => {
      this.setState({saving: false});
      this.handleClose();
    })
    .catch((error) => {
      this.setState({saving: false});
      console.log(error);
    })
  }

  render() {

    const actions = [
      <FlatButton
        label="Cancel"
        primary={true}
        onTouchTap={this.handleClose}
        style={{color: '#CCC', marginRight: '10px' }}
        hoverColor="white"
      />,
      <RaisedButton label={!this.state.saving ? 'Save' : 'Saving'} disabled={this.state.saving} onTouchTap={this.updateAmenity} backgroundColor="#1D89E4" labelColor="#FFF"/>
    ];

    return (
      <div>
        {this.state.loaded &&
        <Dialog title="Edit Amenity"  
          actions={actions}
          modal={true}
          className="editAmenityDialog"
          open={this.state.open}
          onRequestClose={this.handleClose}
          autoScrollBodyContent={true}
          autoDetectWindowHeight={true}
          repositionOnUpdate={true}
          contentStyle={{width: '25%',maxWidth: 'none'}}
        >
        
        <Row>
            <Col xs>
                <TextField floatingLabelText="Amenity Name" floatingLabelFixed={true} fullWidth={true}  defaultValue={this.state.amenity.amenity} ref={(input) => this.amenity.amenity_name = input}/>
            </Col>
        </Row>

        <Row>
            <Col xs>
                <span style={{color: 'rgba(0, 0, 0, 0.3)', transform: 'scale(0.75) translate(0px, 0px)', transformOrigin: 'left top 0px', display: 'block', height: '20px', paddingBottom: '10px'}}>Upload Image</span>
                <Dropzone
                onDrop={this.onDrop.bind(this)} //<= Here
                className='dropzone'
                activeClassName='active-dropzone'
                multiple={false}
                >
                    {this.state.imageFiles.length === 0 && this.state.amenity.item_image &&
                      <div style={{backgroundImage: 'url("http://testingapi.compass.engagiscreatives.net/public/uploads/trans.png")',width: '200px', height: '150px'}}><img key="img-preview" alt="" style={{width: '200px', height: '150px', backgroundSize: 'contain', backgroundPosition: 'center', backgroundImage: `url('${window.public}/${this.state.amenity.item_image}')`, backgroundRepeat: 'no-repeat'}} /></div>
                    }
                    {this.state.imageFiles.length > 0 && this.state.amenity.item_image &&
                        <div style={{backgroundImage: 'url("http://testingapi.compass.engagiscreatives.net/public/uploads/trans.png")',width: '200px', height: '150px'}}>{this.state.imageFiles.map((file) => <img key="img-preview" alt="" style={{width: '200px', height: '150px', backgroundSize: 'cover', backgroundPosition: 'center', backgroundImage: `url('${file.preview}')`, backgroundRepeat: 'no-repeat'}} /> )}</div>
                    }
                    { this.state.imageFiles.length > 0 && this.state.amenity.item_image === "" &&
                        <div style={{backgroundImage: 'url("http://testingapi.compass.engagiscreatives.net/public/uploads/trans.png")',width: '200px', height: '150px'}}>{this.state.imageFiles.map((file) => <img key="img-preview" alt="" style={{width: '200px', height: '150px', backgroundSize: 'cover', backgroundPosition: 'center', backgroundImage: `url('${file.preview}')`, backgroundRepeat: 'no-repeat'}} /> )}</div>
                    }
                    {this.state.imageFiles.length === 0 && this.state.amenity.item_image === "" &&
                        <div style={{width: '200px', border: 'solid 1px #CCC', height: '150px', color: '#CCC', fontSize: '10px'}}><div style={{padding: '20px'}}>Drag and drop or click to select file to upload.</div></div>
                                }
                </Dropzone>
            </Col>
        </Row>

        <Row>
            <Col xs>
                <TextField floatingLabelText="Description" floatingLabelFixed={true} fullWidth={true} defaultValue={this.state.amenity.amenity_description} ref={(input) => this.amenity.amenity_description = input}/>
            </Col>
        </Row>
        </Dialog>
        }
      </div>
    );
  }
}
