import React from 'react';

import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';
// import SelectField from 'material-ui/SelectField';
// import MenuItem from 'material-ui/MenuItem';
import { Row, Col } from 'react-flexbox-grid';

const customContentStyle = {
  width: '25%',
  maxWidth: 'none',
};


export default class AddKiosk extends React.Component {
  state = {
    saving: false,
    submitting: false,
    open: this.props.open,
    value: 2,
  };

  kiosk = {
  };

  handleOpen = () => {
    this.setState({open: true, submitting: false});
  };

  handleClose = () => {
    // this.setState({open: false});
    this.props.hideAddKioskModal();
    this.setState({saving: false});
  };

  handleChange = (event, index, value) => this.setState({value});

  componentWillReceiveProps = (nextProps) => {
      this.setState({open: nextProps.open, submitting: false});
  }

  createKiosk = () => {

    this.setState({saving: true});

    const kiosk = {
      kiosk_name: this.kiosk.kiosk_name.getValue(),
      kiosk_description: this.kiosk.kiosk_description.getValue(),
      is_active: '1'
    }

    var data = new FormData()
    data.append('data', JSON.stringify(kiosk));

    window.fetch(window.endpoint + '/kiosks', {
      method: 'POST',
      body: data
    })

    .then((response) => {
      this.handleClose();
      return response.text();
    })
    .then((body) => {
      this.setState({saving: false}, );
    })
    .catch((error) => {
      this.setState({saving: false});
      console.log(error);
    })
  }

  render() {
    const actions = [
      <FlatButton
        label="Cancel"
        primary={true}
        onTouchTap={this.handleClose}
        style={{color: '#CCC', marginRight: '10px' }}
        hoverColor="white"
      />,
      <RaisedButton label={!this.state.saving ? 'Save' : 'Saving'} disabled={this.state.saving || !this.state.submitting} onTouchTap={this.createKiosk} backgroundColor="#1D89E4" labelColor="#FFF"/>
    ];

    return (
      <div>
        <Dialog title="Add Kiosk"  
          actions={actions}
          modal={true}
          className="addKioskDialog"
          open={this.state.open}
          onRequestClose={this.handleClose}
          autoScrollBodyContent={true}
          autoDetectWindowHeight={true}
          repositionOnUpdate={true}
          contentStyle={customContentStyle}
        >
        
        <Row>
            <Col xs>
                <TextField floatingLabelText="Kiosk Name" floatingLabelFixed={true} fullWidth={true} onChange={(e, newValue) => { this.setState({submitting: newValue === '' ? false : true}) }}   ref={(input) => this.kiosk.kiosk_name = input}/>
            </Col>
        </Row>

         <Row>
            <Col xs>
                <TextField floatingLabelText="Kiosk Description" floatingLabelFixed={true} fullWidth={true} ref={(input) => this.kiosk.kiosk_description = input}/>
            </Col>
        </Row>
            
        
        </Dialog>
      </div>
    );
  }
}
