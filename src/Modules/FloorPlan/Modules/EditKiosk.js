import React from 'react';
// import SelectField from 'material-ui/SelectField';
// import MenuItem from 'material-ui/MenuItem';

import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';

import { Row, Col } from 'react-flexbox-grid';

const customContentStyle = {
  width: '25%',
  maxWidth: 'none',
};

export default class EditKiosk extends React.Component {
  state = {
    loaded: false,
    open: this.props.open,
    value: 2,
    kiosk: {},
  };

  kiosk = {};

  handleChange = (event, index, value) => this.setState({value});

  handleOpen = () => {
    this.setState({open: true});
  };

  handleClose = () => {
    this.setState({open: false});
    this.props.hideEditKioskModal();
  };

  componentWillReceiveProps = (nextProps) => {
      this.setState({open: nextProps.open, loaded: false});
      if(this.props.editKioskID !== nextProps.editKioskID && nextProps.editKioskID !== null){
        window.fetch(window.endpoint + '/kiosks/' + nextProps.editKioskID)
        .then((response) => {
          return response.text();
        })
        .then((body) => {
          const response = JSON.parse(body);
          if(response.status || response.status === false){}
          else {
            this.setState({loaded: true, kiosk: response});
          }
        })
        .catch((e) => {
          console.log(e);
        })
      }
  }

  updateKiosk = () => {

    const kiosk = {
      kiosk_name: this.kiosk.kiosk_name.getValue(),
      kiosk_description: this.kiosk.kiosk_description.getValue(),
      is_active: '1'
    }

    var data = new FormData()
    data.append('data', JSON.stringify(kiosk));

    // if(this.state.imageFiles.length > 0){
    //   data.append('file', this.state.imageFiles[0]);
    // }
    window.fetch(window.endpoint + '/kiosks/' + this.props.editKioskID, {
      method: 'POST',
      body: data
    })
    .then((response) => {
      return response.text();
    })
    .then((body) => {
      this.handleClose();
    })
    .catch((error) => {
      console.log(error);
    })
  }

  render() {
    const actions = [
      <FlatButton
        label="Cancel"
        primary={true}
        onTouchTap={this.handleClose}
        style={{color: '#CCC', marginRight: '10px' }}
        hoverColor="white"
      />,
      <RaisedButton label="Save" onTouchTap={this.updateKiosk} backgroundColor="#1D89E4" labelColor="#FFF"/>
    ];

    return (
      <div>
        {this.state.loaded &&
        <Dialog title="Edit Kiosk"
          ref="dialog"  
          actions={actions}
          modal={true}
          className="editKioskDialog"
          open={this.state.open}
          onRequestClose={this.handleClose}
          autoScrollBodyContent={true}
          autoDetectWindowHeight={true}
          repositionOnUpdate={true}
          contentStyle={customContentStyle}
        >
        
        <Row>
            <Col xs>
                <TextField floatingLabelText="Kiosk Name" floatingLabelFixed={true} fullWidth={true} defaultValue={this.state.kiosk.kiosk_name} ref={(input) => this.kiosk.kiosk_name = input}/>
            </Col>
        </Row>

         <Row>
            <Col xs>
                <TextField floatingLabelText="Kiosk Description" floatingLabelFixed={true} fullWidth={true} defaultValue={this.state.kiosk.kiosk_description} ref={(input) => this.kiosk.kiosk_description = input}/>
            </Col>
        </Row>
            
        </Dialog>
        }
      </div>
    );
  }
}
