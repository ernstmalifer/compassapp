import React from 'react';

import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';

import Dropzone from 'react-dropzone';

import { Row, Col } from 'react-flexbox-grid';

export default class AddAmenity extends React.Component {
  state = {
    saving: false,
    submitting: false,
    open: this.props.open,
    imageFiles: [],
  };

  amenity = {
  };

  handleOpen = () => {
    this.setState({open: true, submitting: false});

  };

  handleClose = () => {
    // this.setState({open: false});
    this.props.hideAddAmenityModal();
    this.setState({saving: false, imageFiles: []});
  };

  onDrop = (imageFiles) => {
    this.setState({
        imageFiles: imageFiles
    })
    console.log(imageFiles)  
    }

  componentWillReceiveProps = (nextProps) => {
      this.setState({open: nextProps.open, submitting: false});
  }

  createAmenity = (event) => {

    this.setState({saving: true});

      const amenity = {
        amenity_name: this.amenity.amenity_name.getValue(),
        amenity_description: this.amenity.amenity_description.getValue(),
        is_active: '1'
      }

      var data = new FormData()
      data.append('data', JSON.stringify(amenity));

      if(this.state.imageFiles.length > 0){
        data.append('file', this.state.imageFiles[0]);
      }
      window.fetch(window.endpoint + '/amenities', {
        method: 'POST',
        body: data
      })
      .then((response) => {
        this.handleClose();
        return response.text();
      })
      .then((body) => {
        this.setState({saving: false});
      })
      .catch((error) => {
        this.setState({saving: false});
        console.log(error);
      })

  }

  render() {
    const actions = [
      <FlatButton
        label="Cancel"
        primary={true}
        onTouchTap={this.handleClose}
        style={{color: '#CCC', marginRight: '10px' }}
        hoverColor="white"
      />,
      <RaisedButton label={!this.state.saving ? 'Save' : 'Saving'} disabled={this.state.saving || !this.state.submitting} onTouchTap={this.createAmenity} backgroundColor="#1D89E4" labelColor="#FFF"/>
    ];

    return (
      <div>
        <Dialog title="Add Amenity"  
          actions={actions}
          modal={true}
          className="addAmenityDialog"
          open={this.state.open}
          onRequestClose={this.handleClose}
          autoScrollBodyContent={true}
          autoDetectWindowHeight={true}
          repositionOnUpdate={true}
          contentStyle={{width: '25%',maxWidth: 'none'}}
        >
        
        <Row>
            <Col xs>
                <TextField floatingLabelText="Amenity Name" floatingLabelFixed={true} fullWidth={true} onChange={(e, newValue) => { this.setState({submitting: newValue === '' ? false : true}) }}  ref={(input) => this.amenity.amenity_name = input}/>
            </Col>
        </Row>

        <Row>
            <Col xs>
                <span style={{color: 'rgba(0, 0, 0, 0.3)', transform: 'scale(0.75) translate(0px, 0px)', transformOrigin: 'left top 0px', display: 'block', height: '20px', paddingBottom: '10px'}}>Upload Image</span>
                <Dropzone
                onDrop={this.onDrop.bind(this)} //<= Here
                className='dropzone'
                activeClassName='active-dropzone'
                multiple={false}
                >
                    {this.state.imageFiles.length > 0 &&
                        <div>{this.state.imageFiles.map((file, idx) => <img key={`upload-${idx}`} alt="" style={{width: '200px', height: '150px', backgroundSize: 'cover', backgroundPosition: 'center', backgroundImage: `url('${file.preview}')`}} /> )}</div>
                    }
                    {this.state.imageFiles.length === 0 &&
                        <div style={{width: '200px', border: 'solid 1px #CCC', height: '150px', color: '#363636', fontSize: '10px'}}><div style={{padding: '20px'}}>Drag and drop or browse files.</div></div>
                    }
                </Dropzone>
            </Col>
        </Row>

        <Row>
            <Col xs>
                <TextField floatingLabelText="Description" floatingLabelFixed={true} fullWidth={true} ref={(input) => this.amenity.amenity_description = input}/>
            </Col>
        </Row>
        </Dialog>
      </div>
    );
  }
}
