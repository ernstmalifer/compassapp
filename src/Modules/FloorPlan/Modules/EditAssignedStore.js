import React from 'react';

import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';

import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';

import { Row, Col } from 'react-flexbox-grid';


const customContentStyle = {
  width: '25%',
  maxWidth: 'none',
};


export default class EditAssignedStore extends React.Component {
  state = {
    loaded: false,
    saving: false,
    open: false,
    assignedstore: null,
    value: 3,
    myValue: 'test_value_placeholder',
    storeFloorplanId: null,
  };

  assignedstore = {};

  handleChange = (event, index, value) => this.setState({value});

  handleClose = () => {
    this.setState({open: false});
    this.props.hideEditAssignedStoreModal();
  };

  updateStoreAssignment = () => {


    this.setState({saving: true});

    this.props.setAssignedStoreFromPanel(this.assignedstore.polygonID.getValue(), this.state.assignedstore.store_id, this.state.storeFloorplanId, this.props.editAssignedStoreId);
    this.handleClose();

  }

  componentWillReceiveProps = (nextProps) => {
    // Load store from API here
      if(this.props.editAssignedStoreId !== nextProps.editAssignedStoreId && nextProps.editAssignedStoreId !== null){
        window.fetch(window.endpoint + '/storeassignments/' + nextProps.editAssignedStoreId)
        .then((response) => {
          return response.text();
        })
        .then((body) => {
          const response = JSON.parse(body);
          if(response.status || response.status === false){}
          else {
            this.setState({open: true, loaded: true, assignedstore: response, storeFloorplanId: response.floor_plan_id});
          }
        })
        .catch((e) => {
          console.log(e);
        })
      }
  }

  handleFloorplanChange = (event, index, value) => this.setState({storeFloorplanId: value});

  render() {
    const actions = [
      <FlatButton
        label="Cancel"
        primary={true}
        onTouchTap={this.handleClose}
        style={{color: '#CCC', marginRight: '10px' }}
        hoverColor="white"
      />,
      <RaisedButton label="Save" disabled={this.state.saving} onTouchTap={this.updateStoreAssignment} backgroundColor="#1D89E4" labelColor="#FFF"/>
    ];

    return (
      <div>
      {this.state.loaded &&
        <Dialog title={this.state.assignedstore.store.store_name}  
          actions={actions}
          modal={true}
          className="editAssignedStoreDialog"
          open={this.state.open}
          onRequestClose={this.handleClose}
          autoScrollBodyContent={true}
          autoDetectWindowHeight={true}
          repositionOnUpdate={true}
          contentStyle={customContentStyle}
        >

        <Row>
          <Col xs>
              <SelectField floatingLabelText="Floorplan" value={this.state.storeFloorplanId} fullWidth={true} onChange={this.handleFloorplanChange}>
                  {this.props.floorplans.map((floorplan, idx)=> {
                  return <MenuItem key={`floorplan-${floorplan.floorplan_id}`} value={floorplan.floorplan_id} primaryText={floorplan.level} />
                })}
              </SelectField>
          </Col>
        </Row>

        <Row>
            <Col xs>
                <TextField floatingLabelText="Room Number" floatingLabelFixed={true} fullWidth={true} defaultValue={this.state.assignedstore.room_name} ref={(input) => this.assignedstore.polygonID = input} />
            </Col>
        </Row>

        </Dialog>
      }
      </div>
    );
  }
}
