import React from 'react';

import {
  NavLink
} from 'react-router-dom';

import { Row, Col } from 'react-flexbox-grid';

import AddFloorplan from './AddFloorplan';

import {
  Table,
  TableBody,
  TableRow,
  TableRowColumn,
} from 'material-ui/Table';
import RaisedButton from 'material-ui/RaisedButton';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import ContentAdd from 'material-ui/svg-icons/content/add';
import ActionZoomIn from 'material-ui/svg-icons/action/zoom-in';
import ActionZoomOut from 'material-ui/svg-icons/action/zoom-out';

// const sampleFloorPlan = [
//   {
//     "floorplanPolygons": [],
//     "name": "Ground Floor",
//     "level": "1"
//   }
// ]

export default class FloorPlans extends React.Component {

  constructor(props){
    super(props);
    this.state = {
      floorplans: this.props.floorplans,
      currentLevel: this.props.floorplans.length ? 0 : null
      // floorplans: sampleFloorPlan
    }
  }

  componentDidReceiveProps = () => {
    this.setState({currentLevel: this.props.floorplans.length - 1});
  }

  setCurrentLevel = (level, e) => {
    this.props.setCurrentLevel(level)
    // this.setState({currentLevel: level});
  }

  editFloorPlan = (floorplan) => {
    // console.log(id);
    this.props.editFloorPlan(floorplan);
  }

  updateFloorPlan = (floorplan) => {
    this.props.updateFloorPlan(floorplan);
  }

  removeFloorPlan = (id) => {
    // const toupdate = update(this.state, { floorplans: { $splice: [[idx,1]]}});
    // this.setState(toupdate);
    console.log(id);
    this.props.removeFloorPlan(id);
  }

  setEmptyTempFoorplan = () => {
    this.props.setTempFloorPlan([]);
  }

  zoomIn = () => {
    this.props.zoomIn();
  }
  
  zoomOut = () => {
    this.props.zoomOut();
  }

  render() {

    let floorPlansPolygons = [];
    if(this.props.floorplans.length > 0) {
      floorPlansPolygons = this.props.floorplans.map((floorplan, floorplanidx) => {
          return floorplan.floorplanPolygons.map((polygon, idx) => {
            const pathStyle = {
              fill: '#78909C',
              strokeWidth: '.5',
              stroke:'#CCC'
            }
            return <path key={polygon.getAttribute('id')} id={polygon.getAttribute('id')} style={pathStyle} d={polygon.getAttribute('d')} />
        });
      });
    }

    return (
      <Row style={{height: '100%'}}>
          {floorPlansPolygons.length > 0 &&
            <Row style={{height: '100%', width: '100%'}}>
              <Col style={{padding: '20px', position: 'relative', flex: `0 0 250px`, background: '#424242', color: 'white', borderRight: '1px solid #ececec'}}>

              <Row>
              <Col xs={9}>
                <h2>Floorplan</h2>
                <p>Lorem ipsum sit amet</p>
              </Col>
              <Col xs={3}>
                <NavLink to={`${this.props.match.url}/add/new`}>
              <FloatingActionButton mini={true} style={{ margin: '10px 0'}} onTouchTap={this.setEmptyTempFoorplan.bind(this)}>
                  <ContentAdd />
                </FloatingActionButton>
                </NavLink>
              </Col>
            </Row>
            
            <div style={{ marginBottom: '20px'}}></div>

                <Table wrapperStyle={{marginLeft: '-20px', marginRight: '-20px'}}>
                  <TableBody displayRowCheckbox={false}>
                  {this.props.floorplans.map( (floorplan, idx) => {
                    const rowClassName = idx % 2 === 0 ? 'odd' : 'even';
                    return <TableRow key={`floorplan-${idx}`} className={rowClassName}>
                      <TableRowColumn style={{width:'5px', padding: '0 20px'}}>{idx + 1}</TableRowColumn>
                      <TableRowColumn>{floorplan.name}</TableRowColumn>
                      <TableRowColumn style={{padding: 0, width: '50px'}}>
                      <NavLink key={'cancelNewFloorPlan'} to={`/app/floorplan/edit`}>
                        <IconButton tooltip="Edit">
                          <EditFloorplanIcon onTouchTap={this.editFloorPlan.bind(this,floorplan)}/>
                        </IconButton>
                      </NavLink>
                      </TableRowColumn>
                      <TableRowColumn style={{padding: 0, width: '50px'}}>
                        <IconButton tooltip="Edit" onTouchTap={this.removeFloorPlan.bind(this,floorplan.floorplan_id)}>
                          <RemoveFloorplanIcon />
                        </IconButton>
                      </TableRowColumn>
                    </TableRow>
                  })}
                  </TableBody>
                </Table>

              </Col>
              <Col xs style={{padding: '20px', position: 'relative',  background: 'linear-gradient(to bottom, rgba(255,255,255,1) 0%, rgba(246,246,246,1) 47%, rgba(237,237,237,1) 100%)'}}>
                <div style={{height: '100%'}}>
                  {floorPlansPolygons.map((polygon, idx) => {
                    const display = idx === this.props.currentLevel ? '' : 'none';
                    return (
                        <svg width="100%" height="100%" key={`floorplan-${idx}`} style={{display: display, zoom: this.props.zoom}}>
                          {polygon}
                        </svg>
                    )
                  })}
                </div>
                <div style={{width: '50px', position: 'absolute', right: '20px', top: '20px' }}>
                  {this.props.floorplans.map( (floorplan, idx) => {
                    return <RaisedButton onTouchTap={this.setCurrentLevel.bind(this, idx)} primary={this.props.currentLevel === idx ? true : false} key={`switchfloorplan-${idx}`} label={`${floorplan.level}`} style={{minWidth: '40px', margin: '5px'}} />
                  })}
                </div>
                <div style={{width: '50px', position: 'absolute', right: '20px', bottom: '20px' }}>
                  <FloatingActionButton mini={true} style={{float: 'right', margin: '10px 0'}} onTouchTap={this.zoomIn.bind(this)}>
                    <ActionZoomIn />
                  </FloatingActionButton>
                  <FloatingActionButton mini={true} style={{float: 'right', margin: '10px 0'}}  onTouchTap={this.zoomOut.bind(this)}>
                    <ActionZoomOut />
                  </FloatingActionButton>
                </div>
              </Col>
            </Row>
            }
            {floorPlansPolygons.length === 0 &&
            <Col xs style={{height: 'inherit', padding: '0px' , background: 'linear-gradient(to bottom, rgba(255,255,255,1) 0%, rgba(246,246,246,1) 47%, rgba(237,237,237,1) 100%)'}}>
              <div style={{height: 'inherit'}}>
              <AddFloorplan setTempFloorPlan={this.props.setTempFloorPlan} zoom={this.state.zoom} zoomIn={this.zoomIn} zoomOut={this.zoomOut} {...this.props} initial={true} />
              </div>
            </Col>
            }
      </Row>
    )
  }
}

import IconButton from 'material-ui/IconButton';
import SvgIcon from 'material-ui/SvgIcon';

const EditFloorplanIcon = (props) => (
  <SvgIcon {...props}>
    <g transform="matrix(.75,0,0,.75,2,0)">
    <path d="M10.681 18.207l-2.209 5.67 5.572-2.307-3.363-3.363zM26.855 6.097l-0.707-0.707c-0.78-0.781-2.047-0.781-2.828 0l-1.414 1.414 3.535 3.536 1.414-1.414c0.782-0.781 0.782-2.048 0-2.829zM10.793 17.918l0.506-0.506 3.535 3.535 9.9-9.9-3.535-3.535 0.707-0.708-11.113 11.114zM23.004 26.004l-17.026 0.006 0.003-17.026 11.921-0.004 1.868-1.98h-14.805c-0.552 0-1 0.447-1 1v19c0 0.553 0.448 1 1 1h19c0.553 0 1-0.447 1-1v-14.058l-2.015 1.977 0.054 11.085z"></path>
    </g>
  </SvgIcon>
)

const RemoveFloorplanIcon = (props) => (
  <SvgIcon {...props}>
    <g transform="matrix(.6,0,0,.6,3,3)">
    <path d="M4 10v20c0 1.1 0.9 2 2 2h18c1.1 0 2-0.9 2-2v-20h-22zM10 28h-2v-14h2v14zM14 28h-2v-14h2v14zM18 28h-2v-14h2v14zM22 28h-2v-14h2v14z"></path>
    <path d="M26.5 4h-6.5v-2.5c0-0.825-0.675-1.5-1.5-1.5h-7c-0.825 0-1.5 0.675-1.5 1.5v2.5h-6.5c-0.825 0-1.5 0.675-1.5 1.5v2.5h26v-2.5c0-0.825-0.675-1.5-1.5-1.5zM18 4h-6v-1.975h6v1.975z"></path>
    </g>
  </SvgIcon>
)