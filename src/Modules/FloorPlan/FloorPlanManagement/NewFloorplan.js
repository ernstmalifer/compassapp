import React from 'react';

import {
  NavLink,
  Redirect
} from 'react-router-dom';

import AddFloorplan from './AddFloorplan';

import { Row, Col } from 'react-flexbox-grid';

import TextField from 'material-ui/TextField';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import FloatingActionButton from 'material-ui/FloatingActionButton';
import ActionZoomIn from 'material-ui/svg-icons/action/zoom-in';
import ActionZoomOut from 'material-ui/svg-icons/action/zoom-out';

import update from 'immutability-helper';

export default class NewFloorplan extends React.Component {

  constructor(props){
    super(props);
    this.state ={
      savedFloorPlan: false,
      canSubmit: false,
      submitting: false,
      name: { error: true, errorText: ''},
      level: { error: true, errorText: ''},
      floorplan: { error: Object.keys(this.props.tempFloorPlan).length > 0 ? false : true, errorText: ''}
    }
  }

  componentWillMount(){
    this.validateFormFields();
  }

  componentWillReceiveProps(nextProps){
    if(Object.keys(nextProps.tempFloorPlan).length > 0){
     this.setState(update(this.state, {floorplan: { error: {$set: false }}}), () => { this.validateFormFields(); });
    }
  }

  enableButton = () => {
    this.setState({ canSubmit: true });
  }

  disableButton = () => {
    this.setState({ canSubmit: false });
  }

  floorPlanNameChange = (event) => {
    if(event.target.value.length === 0){
      this.setState(update(this.state, {name: { error: {$set: true}, errorText: {$set: 'Please enter a floorplan level'}}}), () => { this.validateFormFields(); });
    } else {
      this.setState(update(this.state, {name: { error: {$set: false}, errorText: {$set: ''}}}), () => { this.validateFormFields(); });
    }
  }

  levelChange = (event, value) => {
    if(event.target.value.length === 0){
      this.setState(update(this.state, {level: { error: {$set: true}, errorText: {$set: 'Please enter a level'}}}), () => { this.validateFormFields(); });
    } else {
      const levels = this.props.floorplans.map(o => o['level']);
      if(levels.indexOf(value) !== -1){
        this.setState(update(this.state, {level: { error: {$set: true}, errorText: {$set: 'Please enter a new level'}}}), () => { this.validateFormFields(); });
      } else {
        this.setState(update(this.state, {level: { error: {$set: false}, errorText: {$set: ''}}}), () => { this.validateFormFields(); });
      }
    }
  }

  validateFormFields = () => {
    if(!this.state.name.error && !this.state.level.error && !this.state.floorplan.error ){
      this.enableButton();
    } else {
      this.disableButton();
    }
  }

  saveNewFloorPlan = (event) => {

    this.setState({submitting: true});

    let saveTempFloorPlan = Object.assign({}, this.props.tempFloorPlan);
    saveTempFloorPlan.name = this.refs.name.getValue();
    saveTempFloorPlan.level = this.refs.level.getValue();

    // this.props.saveTempFloorPlan(saveTempFloorPlan);

    const floorplan = {
      floorplan_name: saveTempFloorPlan.name,
      floorplan_level: saveTempFloorPlan.level,
      is_active: '1'
    }

    var data = new FormData()
    data.append('data', JSON.stringify(floorplan));

    if(window.tempFloorPlanFile){
      data.append('file', window.tempFloorPlanFile);
    }
    window.fetch(window.endpoint + '/floorplans', {
      method: 'POST',
      body: data
    })
    .then((response) => {
      return response.text();
    })
    .then((body) => {
      window.tempFloorPlanFile = null;
      this.props.saveTempFloorPlan();
      this.setState({savedFloorPlan: true});
    })
    .catch((error) => {
      console.log(error);
    })
    

  }

  zoomIn = () => {
    this.props.zoomIn();
  }
  
  zoomOut = () => {
    this.props.zoomOut();
  }

  render() {

    if(this.state.savedFloorPlan){
      let to = this.props.match.url.split('/');
      to.pop();
      to.pop();
      return <Redirect to={to.join('/')} />
    }

    let tempFloorPlanPolygons;
    if (this.props.tempFloorPlan && this.props.tempFloorPlan.floorplanPolygons) {
      tempFloorPlanPolygons = this.props.tempFloorPlan.floorplanPolygons.map((polygon, idx) => {
          const pathStyle = {
            fill: '#78909C',
            strokeWidth: '.5',
            stroke:'#CCC'
          }
          return <path key={polygon.getAttribute('id')} id={polygon.getAttribute('id')} d={polygon.getAttribute('d')} style={pathStyle}/>
        });
    }

    const actions = [ 
      <NavLink key={'cancelNewFloorPlan'} to={`/app/floorplan`}><FlatButton label="Cancel" primary={true} style={{color: '#CCC', marginRight: '10px' }} hoverColor="white" /></NavLink>,
      <RaisedButton type="submit" onTouchTap={this.saveNewFloorPlan} disabled={!this.state.canSubmit || this.state.submitting} key={`saveTempFloorPlan`} label={this.state.submitting ? 'Saving' : 'Save'} backgroundColor="#1D89E4" labelColor="#FFF" />
    ];

    return (
      <Row style={{height: '100%'}}>
        <Col style={{padding: '20px', position: 'relative', flex: `0 0 250px`}}>
          <h2>Add Floorplan</h2>
          <div>
            <TextField
              style={{width: '250px'}}
              floatingLabelText="Floorplan Name"
              floatingLabelFixed={true}
              errorText={this.state.name.errorText}
              onChange={this.floorPlanNameChange.bind(this)}
              ref="name"/>
          </div>
          <div>
            <TextField style={{width: '250px'}} floatingLabelText="Level" floatingLabelFixed={true} errorText={this.state.level.errorText} onChange={this.levelChange.bind(this)} ref="level" />
          </div>
          <div style={{position: 'absolute', bottom: '20px', right: '20px'}}>
            {actions}
          </div>
        </Col>
        <Col xs style={{padding: '20px', background: 'linear-gradient(to bottom, rgba(255,255,255,1) 0%, rgba(246,246,246,1) 47%, rgba(237,237,237,1) 100%)'}}>
        {tempFloorPlanPolygons &&
          <div style={{height: '100%'}}>
          <svg width="100%" height="100%" style={{zoom: this.props.zoom}}>
            {tempFloorPlanPolygons}
          </svg>
          <div style={{width: '50px', position: 'absolute', right: '20px', bottom: '20px' }}>
            <FloatingActionButton mini={true} style={{float: 'right', margin: '10px 0'}} onTouchTap={this.zoomIn.bind(this)}>
              <ActionZoomIn />
            </FloatingActionButton>
            <FloatingActionButton mini={true} style={{float: 'right', margin: '10px 0'}}  onTouchTap={this.zoomOut.bind(this)}>
              <ActionZoomOut />
            </FloatingActionButton>
          </div>
          </div>
        }
        {!tempFloorPlanPolygons &&
          <AddFloorplan setTempFloorPlan={this.props.setTempFloorPlan} zoom={this.state.zoom} zoomIn={this.zoomIn} zoomOut={this.zoomOut} {...this.props} />
        }
        </Col>
      </Row>
    )
  }
}