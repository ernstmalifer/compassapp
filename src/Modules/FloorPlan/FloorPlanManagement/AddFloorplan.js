import React from 'react';

import {
  Redirect
} from 'react-router-dom';

import logo from '../assets/floorplan_ico.svg';
import RaisedButton from 'material-ui/RaisedButton';

export default class AddFloorplan extends React.Component {

  constructor(props){
    super(props);
    this.state = {newFloorPlan: false};
  }

  openFileDialog = () =>{
    this.refs.fileUpload.click();
  }

  addTempFloorPlan = (event) => {

    var reader = new FileReader();
    var parser = new DOMParser();

    window.tempFloorPlanFile = event.target.files[0];

    reader.readAsText(event.target.files[0]);
    reader.onloadend = () => {
      const xmlData = parser.parseFromString(reader.result, "application/xml");
      const polygons = xmlData.getElementsByTagName('path');

      let tempFloorPlan = {  floorplanPolygons: [] };
      for (let i = 0; i < polygons.length; i++) {
        tempFloorPlan.floorplanPolygons.push(polygons[i]);
      }

      this.refs.addFloorPlan.reset();

      this.props.setTempFloorPlan(tempFloorPlan, );
      this.setState({newFloorPlan: true});

    };
  }

  render() {

    if(this.state.newFloorPlan && this.props.initial){
      return <Redirect to={`${this.props.match.url}/add/new`} />
    }

    if(this.state.newFloorPlan && !this.props.initial){
      return <Redirect to={`${this.props.match.url}/new`} />
    }

    return (
      <div style={{height: '100%', position: 'relative'}}>
        <div style={{textAlign: 'center', position:'absolute', width: '100%', height: '150px', top: '50%', marginTop: '-75px'}}>
          <img src={logo} className="logo_loading" alt="Loading"/>
          <h3 style={{marginTop: '0px', marginBottom: '10px'}}>
            {this.props.initial && <span>Upload Floorplan to get started.</span>}
            {!this.props.initial && <span>Upload Floorplan</span>}
          </h3>
          <form ref="addFloorPlan" style={{textAlign: 'center'}}>
            
            <RaisedButton label="Browse" onClick={this.openFileDialog} backgroundColor="#1D89E4" labelColor="#FFF"/>
            <input ref="fileUpload" type="file" style={{"display" : "none"}} onChange={this.addTempFloorPlan.bind(this)} accept=".svg"/>

          </form>
        </div>
      </div>
    )
  }
}