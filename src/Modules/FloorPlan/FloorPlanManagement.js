import React from 'react';

import {
  Route,
} from 'react-router-dom';

import Floorplans from './FloorPlanManagement/Floorplans';
import AddFloorplan from './FloorPlanManagement/AddFloorplan';
import NewFloorplan from './FloorPlanManagement/NewFloorplan';
import EditFloorplan from './FloorPlanManagement/EditFloorplan';

import './index.css';

export default class FloorPlanManagement extends React.Component {

  constructor(props){
    super(props);

    this.state = {
      // floorplans: [],
      // tempFloorPlan: {},
      // zoom: 1.0,
      floorplans: this.props.floorplans,
      tempFloorPlan: this.props.tempFloorPlan,
      currentLevel: this.props.currentLevel,
      zoom: this.props.zoom
    }
  }

  setTempFloorPlan = (tempFloorPlan) => {
    // this.setState({tempFloorPlan});
    this.props.setTempFloorPlan(tempFloorPlan);
  }

  saveTempFloorPlan = (saveTempFloorPlan) => {
    // const toupdate = update(this.state, { floorplans: { $push: [saveTempFloorPlan]}});
    // this.setState(toupdate);
    // this.setState({tempFloorPlan: {}});
    this.props.saveTempFloorPlan(saveTempFloorPlan);
  }

  editFloorPlan = (floorplan) => {
    this.props.setTempFloorPlan(floorplan);
  }

  removeFloorPlan = (id) => {
    console.log(id);
    // const toupdate = update(this.state, { floorplans: { $splice: [[idx,1]]}});
    // this.setState(toupdate);
    this.props.removeFloorPlan(id);
  }

  zoomIn = () => {
    // this.setState({zoom: this.state.zoom + 0.1})
    this.props.zoomIn();
  }
  
  zoomOut = () => {
    // if(this.state.zoom >= 1){
    //   this.setState({zoom: this.state.zoom - 0.1})
    // }
    this.props.zoomOut();
  }

  setCurrentLevel = (currentLevel) => {
    this.props.setCurrentLevel(currentLevel);
  }

  render() {
    return (
      <div style={{position: 'absolute',height: '100%',width: '100%'}}>
        <Route exact path={`${this.props.match.url}/`} render={(props) => <Floorplans setTempFloorPlan={this.setTempFloorPlan} floorplans={this.props.floorplans} zoom={this.props.zoom} zoomIn={this.zoomIn} zoomOut={this.zoomOut} currentLevel={this.props.currentLevel} setCurrentLevel={this.setCurrentLevel}  removeFloorPlan={this.removeFloorPlan} editFloorPlan={this.editFloorPlan} {...props} />} />
        <Route exact path={`${this.props.match.url}/add`} render={(props) => <AddFloorplan setTempFloorPlan={this.setTempFloorPlan} {...props} initial={false} />} />
        <Route path={`${this.props.match.url}/add/new`} render={(props) => <NewFloorplan floorplans={this.props.floorplans} setTempFloorPlan={this.setTempFloorPlan} tempFloorPlan={this.props.tempFloorPlan} saveTempFloorPlan={this.saveTempFloorPlan} zoom={this.props.zoom} zoomIn={this.zoomIn} zoomOut={this.zoomOut} currentLevel={this.props.currentLevel} setCurrentLevel={this.setCurrentLevel} {...props} />} />
        <Route path={`${this.props.match.url}/edit`} render={(props) => <EditFloorplan floorplans={this.props.floorplans} setTempFloorPlan={this.setTempFloorPlan} tempFloorPlan={this.props.tempFloorPlan} saveTempFloorPlan={this.saveTempFloorPlan} zoom={this.props.zoom} zoomIn={this.zoomIn} zoomOut={this.zoomOut} currentLevel={this.props.currentLevel} setCurrentLevel={this.setCurrentLevel} {...props} />} />
      </div>
    );
  }
}