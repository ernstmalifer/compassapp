import React from 'react';

import {
  Route
} from 'react-router-dom';

import StoreList from './StoreList';
import StoreCategory from './StoreCategory';

import {Card, CardText} from 'material-ui/Card';

export default class StoreDirectory extends React.Component {

  render() {
    return (
      <Card className="MainCard" containerStyle={{ padding: '20px', height: 'inherit', boxSizing: 'border-box' }}>
        <CardText style={{height: 'inherit', position: 'relative', padding: '0px'}}>
        <Route exact path={`${this.props.match.url}/`} component={StoreList} />
        <Route path={`${this.props.match.url}/storecategory`} component={StoreCategory} />
      </CardText>
      </Card>
    );
  }
}