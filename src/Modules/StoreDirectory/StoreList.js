import React from 'react';

import 'whatwg-fetch';
import update from 'immutability-helper';

import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn,
} from 'material-ui/Table';
import Menu from 'material-ui/Menu';
import MenuItem from 'material-ui/MenuItem';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
import DropDownMenu from 'material-ui/DropDownMenu';
import {Toolbar, ToolbarGroup } from 'material-ui/Toolbar';
import Snackbar from 'material-ui/Snackbar';

import CreateStore from './Modules/CreateStore.js';
import EditStore from './Modules/EditStore.js';

import moment from 'moment';

import './StoreList.css';

const tableHeaderTableRowStyle = {
  backgroundColor: '#616161'
}

const tableHeaderColumnStyle = {
  fontSize: '14px',
  color: 'white',
  textTransform: 'uppercase',
  padding: '5px 5px 5px 15px',
  height: '30px'
}

const toolbarStyle = {
  backgroundColor: '#ffffff',
  marginBottom: '20px'
}

// const stores = [
//     { id: '1', store_name: 'Nike', store_contact_number: '555-1212', store_email_address: 'store@nike.com', store_website: 'nike.com', store_description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Saepe, beatae!', store_trading_hours: 'daily', store_trading_hours_daily: {opening_time: '10:35:24-08:00', closing_time: '10:35:24-08:00'}, store_trading_hours_custom: [], store_tags: ['sports', 'shoes'], store_featured: true, store_image: 'https://upload.wikimedia.org/wikipedia/commons/thumb/a/a6/Logo_NIKE.svg/1200px-Logo_NIKE.svg.png', owner_name: 'John Niker', owner_email: 'john@nike.com', owner_contact_number:'555-1212', is_archived: false, store_category: 1 }
// ];

export default class StoreList extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      categoryFilter: 0,
      showCreateStore: false,
      editStoreID: null,
      showEditStore: false,
      stores: [],
      storesSearch: '',
      storesLimit: 10,
      storesOffset: 0,
      storesPages: 0,
      storesCurrentPage: 0,
      storesSortColumn: '',
      storesSortBy: '',
      categories: [],
      snackbar: {
        message: '',
        open: false
      },
      search: ''
    };
  }

  componentWillMount = () => {
    this.loadStores();
    this.loadCategories();
  }

  loadStores = (reset) => {
    this.setState(update(this.state, { snackbar: {message: {$set: 'Loading stores' }, open: {$set: true}}}));

    // stores?where={"s.store_name":{"LIKE": "%o%"},"c.category_id":{"=":"14"}}&sort=store_name%20ASC
    // stores?limit=10&skip=0&where={"s.store_name":{"LIKE":"a"

    // var str = `stores?limit=${this.state.storesLimit}&skip=${this.state.storesOffset}&where={${this.state.storesSearch.length > 0?`"s.store_name":{"LIKE":"${this.state.storesSearch}"}`:''},${this.state.categoryFilter !== 0 ? `"c.category_id":{"=":"${this.state.categoryFilter}"}`:''}}${this.state.storesSortBy.length > 0?`&sort=${this.state.storesSortColumn}%20${this.state.storesSortBy}`:''}`;

    // console.log( str );

    const query = `${window.endpoint}/stores??limit=${this.state.storesLimit}&skip=${this.state.storesOffset}&where={${this.state.storesSearch.length > 0?`"s.store_name":{"LIKE":"%${this.state.storesSearch}%"}`:''}${this.state.categoryFilter !== 0 && this.state.storesSearch.length > 0 ? `,`:``}${this.state.categoryFilter !== 0 ? `"c.category_id":{"=":"${this.state.categoryFilter}"}`:''}}${this.state.storesSortBy.length > 0?`&sort=${this.state.storesSortColumn} ${this.state.storesSortBy}`:''}`;


    window.fetch(encodeURI(query)).then((response) => {
      return response.text();
    })
    .then((body) => {
      const response = JSON.parse(body);
      if(response.status || response.status === false){}
      else {
        this.setState({stores: response.stores, storesPages: Math.ceil(parseInt(response.total_rows, 10) / this.state.storesLimit), snackbar: {message: '', open: false}}, ()=> {
          if(reset){
            this.changePage(0);
          }
        });
        
      }
    })
    .catch((e) => {
      console.log(e);
      this.setState(update(this.state, { snackbar: {message: {$set: 'Failed to load stores' }, open: {$set: true}}}));
    })
  }

  loadCategories = () => {
    window.fetch(window.endpoint + '/categories?limit=1000')
    .then((response) => {
      return response.text();
    })
    .then((body) => {
      const response = JSON.parse(body);
      if(response.status || response.status === false){}
      else {
        this.setState({categories: response.categories})
      }
    })
    .catch((e) => {
      console.log(e);
    })
  }

  changePage = (i) => {
    this.setState({storesCurrentPage: i, storesOffset: 0 + (this.state.storesLimit * i) }, () => {
      this.loadStores();
    })
  }

  handleCategoryFilter = (event, index, value) => {
    this.setState({categoryFilter: value}, ()=> {
      this.loadStores(true);
    });
  }

  showCreateStoreModal = (event) => this.setState({showCreateStore: true});

  hideCreateStoreModal = (event) => {
    this.setState({showCreateStore: false}, ()=>{
      this.loadStores();
      this.loadCategories();
    });
  }

  showEditStoreModal = (id) => {
    this.setState({editStoreID: id}, ()=> { this.setState({showEditStore: true}) });
  }

  hideEditStoreModal = (event) => {
    this.setState({editStoreID: null, showEditStore: false}, ()=>{
      this.loadStores();
      this.loadCategories();
    });
  }

  removeStore = (id) => {
    window.fetch(window.endpoint + '/stores/' + id, {method: 'DELETE'})
    .then((response) => {
      return response.text();
    })
    .then((body) => {
      const response = JSON.parse(body);
      if(response.status || response.status === false){}
      else {
        this.loadStores();
        this.loadCategories();
      }
    })
    .catch((e) => {
      console.log(e);
    })
  }

  closeSnackBar = () => {
    this.setState(update(this.state, { snackbar: {open: {$set: false}}}));
  };

  updateSearch = (e) => {
      // console.log(e.target.value);
    this.setState({storesSearch: e.target.value}, () => {
      this.loadStores(true);
    })
  }

  highlight = (str) => {
    if(this.state.storesSearch.length > 0) {
      var regex = new RegExp(this.state.storesSearch  , 'gi');
      return {__html: str.replace(regex, function(str) {return '<span style="color: rgb(29, 137, 228);">'+str+'</span>'})};
    } else {
      return {__html:str};
    }
  }

  sortBy = (column) => {

    let sortby;
    switch(this.state.storesSortBy){
      case '':
        sortby = 'ASC';
        break;
      case 'ASC':
        sortby = 'DESC';
        break;
      case 'DESC':
        sortby = '';
        break;
      default:
        break;
    }

    this.setState({storesSortColumn: column, storesSortBy: sortby}, () => {
      this.loadStores(true);
    })
  }

  render() {

    const pages = [];
    for (var i=0; i < this.state.storesPages; i++) {
        pages.push(<RaisedButton key={`page-${i}`} label={i + 1} primary={i === this.state.storesCurrentPage} onTouchTap={this.changePage.bind(this,i)} style={{minWidth: '20px'}} />);
    }

    return (
      <div style={{position: 'relative', height: '100%'}}>
        <Toolbar className="resToolbar" style={toolbarStyle}>
          <ToolbarGroup className="resMenu" firstChild={true}>
            <Menu disableAutoFocus={true}>
              <MenuItem leftIcon={<SearchIcon style={{marginRight: '0px'}}/>} style={{paddingLeft: '50px'}} disabled={true}>
                <TextField hintText='Search Stores' value={this.state.storesSearch} onChange={this.updateSearch.bind(this)}/>
              </MenuItem>
            </Menu>
            <Menu disableAutoFocus={true}>
              <MenuItem style={{paddingLeft: '0px', lineHeight: '1.5'}} disabled={true}>
                <DropDownMenu value={this.state.categoryFilter} style={{width: '200px'}} onChange={this.handleCategoryFilter}>
                  <MenuItem value={0} primaryText="All Stores" />
                  {this.state.categories.map((category, idx)=> {
                    return <MenuItem key={`filtercategory-${category.category_id}`} value={category.category_id} primaryText={category.category_name} />
                  })}
                </DropDownMenu>
              </MenuItem>
            </Menu>
          </ToolbarGroup>
          <ToolbarGroup lastChild={true} className="resMenu">
            <RaisedButton label="Create Store" onTouchTap={this.showCreateStoreModal} style={{margin: '10px'}} primary={true}/>
          </ToolbarGroup>
        </Toolbar>

        <Table bodyStyle={{marginBottom: '50px'}}>
              <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
                <TableRow style={tableHeaderTableRowStyle}>
                  <TableHeaderColumn onTouchTap={this.sortBy.bind(this,'store_name')} style={Object.assign({}, tableHeaderColumnStyle, {cursor: 'pointer'})}>
                    Store
                    {this.state.storesSortColumn === 'store_name' &&
                      <span>
                      {this.state.storesSortBy === 'ASC' &&
                        <Asc color={'white'} style={{height: '13px'}}/>
                      }
                      {this.state.storesSortBy === 'DESC' &&
                        <Desc color={'white'} style={{height: '13px'}}/>
                      }
                      </span>
                    }
                  </TableHeaderColumn>
                  <TableHeaderColumn style={tableHeaderColumnStyle}>Category</TableHeaderColumn>
                  <TableHeaderColumn onTouchTap={this.sortBy.bind(this,'date_created')} style={Object.assign({}, tableHeaderColumnStyle, {cursor: 'pointer'})}>
                    Date Created
                    {this.state.storesSortColumn === 'date_created' &&
                      <span>
                      {this.state.storesSortBy === 'ASC' &&
                        <Asc color={'white'} style={{height: '13px'}}/>
                      }
                      {this.state.storesSortBy === 'DESC' &&
                        <Desc color={'white'} style={{height: '13px'}}/>
                      }
                      </span>
                    }
                  </TableHeaderColumn>
                  <TableHeaderColumn style={{width: '30px', ...tableHeaderColumnStyle}}></TableHeaderColumn>
                  <TableHeaderColumn style={{width: '30px', ...tableHeaderColumnStyle}}></TableHeaderColumn>
                </TableRow>
              </TableHeader>
              {this.state.stores.length > 0 &&
                <TableBody displayRowCheckbox={false}>
                {this.state.stores.map( (store, storeidx) => {

                  const rowClassName = storeidx % 2 === 0 ? 'odd' : 'even';

                  return <TableRow key={`store-${store.id}`} className={rowClassName}>
                    <TableRowColumn><div dangerouslySetInnerHTML={this.highlight(store.store_name)} /> </TableRowColumn>
                    <TableRowColumn>
                      {store.store_category &&
                        store.store_category.map((category, categoryidx) => {
                          return <span key={`store-${store.id}-category-${category.category_id}`}>{category.category_name}
                            {categoryidx !== store.store_category.length - 1 &&
                              <span>, </span>
                            }
                          </span>
                        })
                      }
                    </TableRowColumn>
                    <TableRowColumn>{`${moment(store.date_created).format('MMM DD YYYY')}`} <em>{`(${moment(store.date_created).fromNow()})`}</em></TableRowColumn>
                    <TableRowColumn  style={{width: '30px'}}>
                      <IconButton tooltip="Edit" onTouchTap={this.showEditStoreModal.bind(this, store.id)}>
                        <EditStoreIcon />
                      </IconButton>
                    </TableRowColumn>
                    <TableRowColumn style={{width: '30px'}}>
                      <IconButton tooltip="Remove">
                        <RemoveStoreIcon onTouchTap={this.removeStore.bind(this, store.id)}/>
                      </IconButton>
                    </TableRowColumn>
                  </TableRow>
                })}
                </TableBody>
              }
              
              {this.state.stores.length === 0 &&
                <TableBody displayRowCheckbox={false}>
                  <TableRow>
                    <TableRowColumn>There are no stores</TableRowColumn>
                  </TableRow>
                </TableBody>
              }

       </Table>
        <div style={{position: 'absolute', right: '0px', bottom: '0px'}}>
          {pages}
        </div>
        <div style={{clear: 'both'}}></div>
        
        <CreateStore open={this.state.showCreateStore} hideCreateStoreModal={this.hideCreateStoreModal} categories={this.state.categories}/>
        <EditStore open={this.state.showEditStore} hideEditStoreModal={this.hideEditStoreModal} editStoreID={this.state.editStoreID} categories={this.state.categories}/>

        <Snackbar
          open={this.state.snackbar.open}
          message={this.state.snackbar.message}
          autoHideDuration={4000}
          onRequestClose={this.closeSnackBar}
        />

      </div>
    );
  }
}

import IconButton from 'material-ui/IconButton';
import SvgIcon from 'material-ui/SvgIcon';

const EditStoreIcon = (props) => (
  <SvgIcon {...props}>
    <g transform="matrix(.75,0,0,.75,2,0)">
    <path d="M10.681 18.207l-2.209 5.67 5.572-2.307-3.363-3.363zM26.855 6.097l-0.707-0.707c-0.78-0.781-2.047-0.781-2.828 0l-1.414 1.414 3.535 3.536 1.414-1.414c0.782-0.781 0.782-2.048 0-2.829zM10.793 17.918l0.506-0.506 3.535 3.535 9.9-9.9-3.535-3.535 0.707-0.708-11.113 11.114zM23.004 26.004l-17.026 0.006 0.003-17.026 11.921-0.004 1.868-1.98h-14.805c-0.552 0-1 0.447-1 1v19c0 0.553 0.448 1 1 1h19c0.553 0 1-0.447 1-1v-14.058l-2.015 1.977 0.054 11.085z"></path>
    </g>
  </SvgIcon>
)

const RemoveStoreIcon = (props) => (
  <SvgIcon {...props}>
    <g transform="matrix(.6,0,0,.6,3,3)">
    <path d="M4 10v20c0 1.1 0.9 2 2 2h18c1.1 0 2-0.9 2-2v-20h-22zM10 28h-2v-14h2v14zM14 28h-2v-14h2v14zM18 28h-2v-14h2v14zM22 28h-2v-14h2v14z"></path>
    <path d="M26.5 4h-6.5v-2.5c0-0.825-0.675-1.5-1.5-1.5h-7c-0.825 0-1.5 0.675-1.5 1.5v2.5h-6.5c-0.825 0-1.5 0.675-1.5 1.5v2.5h26v-2.5c0-0.825-0.675-1.5-1.5-1.5zM18 4h-6v-1.975h6v1.975z"></path>
    </g>
  </SvgIcon>
)

const SearchIcon = (props) => (
  <SvgIcon {...props}>
    <g transform="matrix(.6,0,0,.6,3,3)">
    <path d="M18 13c0-3.859-3.141-7-7-7s-7 3.141-7 7 3.141 7 7 7 7-3.141 7-7zM26 26c0 1.094-0.906 2-2 2-0.531 0-1.047-0.219-1.406-0.594l-5.359-5.344c-1.828 1.266-4.016 1.937-6.234 1.937-6.078 0-11-4.922-11-11s4.922-11 11-11 11 4.922 11 11c0 2.219-0.672 4.406-1.937 6.234l5.359 5.359c0.359 0.359 0.578 0.875 0.578 1.406z"></path>
    </g>
  </SvgIcon>
)

import Asc from 'material-ui/svg-icons/navigation/arrow-drop-up';
import Desc from 'material-ui/svg-icons/navigation/arrow-drop-down';