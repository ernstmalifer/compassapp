import React from 'react';

import 'whatwg-fetch';
import update from 'immutability-helper';

import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn
} from 'material-ui/Table';

import Menu from 'material-ui/Menu';
import MenuItem from 'material-ui/MenuItem';
import TextField from 'material-ui/TextField';
import RaisedButton from 'material-ui/RaisedButton';
// import DropDownMenu from 'material-ui/DropDownMenu';
import {Toolbar, ToolbarGroup } from 'material-ui/Toolbar';
import Snackbar from 'material-ui/Snackbar';

import CreateCategory from './Modules/CreateCategory.js';
import EditCategory from './Modules/EditCategory.js';

import './StoreList.css';

import IconButton from 'material-ui/IconButton';
import SvgIcon from 'material-ui/SvgIcon';

import moment from 'moment';

const EditStoreIcon = (props) => (
  <SvgIcon {...props}>
    <g transform="matrix(.75,0,0,.75,2,0)">
    <path d="M10.681 18.207l-2.209 5.67 5.572-2.307-3.363-3.363zM26.855 6.097l-0.707-0.707c-0.78-0.781-2.047-0.781-2.828 0l-1.414 1.414 3.535 3.536 1.414-1.414c0.782-0.781 0.782-2.048 0-2.829zM10.793 17.918l0.506-0.506 3.535 3.535 9.9-9.9-3.535-3.535 0.707-0.708-11.113 11.114zM23.004 26.004l-17.026 0.006 0.003-17.026 11.921-0.004 1.868-1.98h-14.805c-0.552 0-1 0.447-1 1v19c0 0.553 0.448 1 1 1h19c0.553 0 1-0.447 1-1v-14.058l-2.015 1.977 0.054 11.085z"></path>
    </g>
  </SvgIcon>
)

const RemoveStoreIcon = (props) => (
  <SvgIcon {...props}>
    <g transform="matrix(.6,0,0,.6,3,3)">
    <path d="M4 10v20c0 1.1 0.9 2 2 2h18c1.1 0 2-0.9 2-2v-20h-22zM10 28h-2v-14h2v14zM14 28h-2v-14h2v14zM18 28h-2v-14h2v14zM22 28h-2v-14h2v14z"></path>
    <path d="M26.5 4h-6.5v-2.5c0-0.825-0.675-1.5-1.5-1.5h-7c-0.825 0-1.5 0.675-1.5 1.5v2.5h-6.5c-0.825 0-1.5 0.675-1.5 1.5v2.5h26v-2.5c0-0.825-0.675-1.5-1.5-1.5zM18 4h-6v-1.975h6v1.975z"></path>
    </g>
  </SvgIcon>
)

/*const ArchiveIcon = (props) => (
  <SvgIcon {...props}>
    <path d="M13.981 2h-7.963c0 0-0.996 0-0.996 1h9.955c0-1-0.996-1-0.996-1zM16.968 5c0-1-0.995-1-0.995-1h-11.946c0 0-0.995 0-0.995 1v1h13.936v-1zM18.958 6c-0.588-0.592-0.588-0.592-0.588-0.592v1.592h-16.74v-1.592c0 0 0 0-0.589 0.592s-1.011 0.75-0.774 2c0.236 1.246 1.379 8.076 1.549 9 0.186 1.014 1.217 1 1.217 1h13.936c0 0 1.030 0.014 1.217-1 0.17-0.924 1.312-7.754 1.549-9 0.235-1.25-0.187-1.408-0.777-2zM14 11.997c0 0.554-0.449 1.003-1.003 1.003h-5.994c-0.554 0-1.003-0.449-1.003-1.003v-1.997h1v2h6v-2h1v1.997z"></path>
  </SvgIcon>
)*/

const SearchIcon = (props) => (
  <SvgIcon {...props}>
    <g transform="matrix(.6,0,0,.6,3,3)">
    <path d="M18 13c0-3.859-3.141-7-7-7s-7 3.141-7 7 3.141 7 7 7 7-3.141 7-7zM26 26c0 1.094-0.906 2-2 2-0.531 0-1.047-0.219-1.406-0.594l-5.359-5.344c-1.828 1.266-4.016 1.937-6.234 1.937-6.078 0-11-4.922-11-11s4.922-11 11-11 11 4.922 11 11c0 2.219-0.672 4.406-1.937 6.234l5.359 5.359c0.359 0.359 0.578 0.875 0.578 1.406z"></path>
    </g>
  </SvgIcon>
)

const tableHeaderTableRowStyle = {
  backgroundColor: '#616161'
}

const tableHeaderColumnStyle = {
  fontSize: '14px',
  color: 'white',
  textTransform: 'uppercase',
  padding: '5px 5px 5px 15px',
  height: '30px'
}

const toolbarStyle = {
  backgroundColor: '#ffffff',
  marginBottom: '20px'
}

// const categoriesData = [
//     { categoryName: 'Beddings', dateCreated: 10, categoryID: 123 },
//     { categoryName: 'Furniture', dateCreated: 19, categoryID: 124 },
//     { categoryName: 'Hardware', dateCreated: 21, categoryID: 125 },
//     { categoryName: 'Electronics', dateCreated: 35, categoryID: 126 },
//     { categoryName: 'Fresh Food', dateCreated: 29, categoryID: 127 },
//     { categoryName: 'Clothings', dateCreated: 29, categoryID: 128 },
//     { categoryName: 'Toys', dateCreated: 29, categoryID: 129 },
//     { categoryName: 'Test', dateCreated: 29, categoryID: 130 }
// ];

export default class StoreCategory extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      open: false,
      value: 1,
      showCreateStore: false,
      editCategoryID: null,
      showEditStore: false,
      showCreateCategory: false,
      showEditCategory: false,
      categories: [],
      categoriesLimit: 10,
      categoriesOffset: 0,
      categoriesPages: 0,
      categoriesCurrentPage: 0,
      categoriesSearch: '',
      categoriesSortBy: '',
      categoriesSortColumn: '',
      snackbar: {
        message: '',
        open: false
      },
      search: ''
    };
  }


  componentWillMount = () => {
    this.loadCategories();
  }

  loadCategories = (reset) => {

    const query = `${window.endpoint}/categories??limit=${this.state.categoriesLimit}&skip=${this.state.categoriesOffset}&where={${this.state.categoriesSearch.length > 0?`"item_name":{"LIKE":"%${this.state.categoriesSearch}%"}`:''}}${this.state.categoriesSortBy.length > 0?`&sort=${this.state.categoriesSortColumn} ${this.state.categoriesSortBy}`:''}`;

    this.setState(update(this.state, { snackbar: {message: {$set: 'Loading categories' }, open: {$set: true}}}));
    window.fetch(encodeURI(query))
    // window.fetch(`${window.endpoint}/categories?limit=${this.state.categoriesLimit}&skip=${this.state.categoriesOffset}`)
    .then((response) => {
      return response.text();
    })
    .then((body) => {
      const response = JSON.parse(body);
      if(response.status || response.status === false){}
      else {
        this.setState({categories: response.categories, categoriesPages: Math.ceil(parseInt(response.total_rows, 10) / this.state.categoriesLimit),snackbar: {message: '', open: false}}, ()=> {
          if(reset){
            this.changePage(0);
          }
        })
      }
    })
    .catch((e) => {
      console.log(e);
      this.setState(update(this.state, { snackbar: {message: {$set: 'Failed to load categories' }, open: {$set: true}}}));
    })
  }

  changePage = (i) => {
    this.setState({categoriesCurrentPage: i, categoriesOffset: 0 + (this.state.categoriesLimit * i) }, () => {
      this.loadCategories();
    })
  }

  showCreateCategoryModal = (event) => {
    this.setState({showCreateCategory:true})
  }

  showEditCategoryModal = (id) => {
    this.setState({editCategoryID: id}, ()=> { this.setState({showEditCategory: true}) });
  }

  hideCreateCategoryModal = (event) => {
    this.setState({showCreateCategory:false}, () => {
      this.loadCategories();
    });
  }

  hideEditCategoryModal = (event) => {
    this.setState({editCategoryID: null, showEditCategory:false}, () => {
      this.loadCategories();
    });
  }

  handleChange = (event, index, value) => this.setState({value});

  removeCategory = (id) => {
    window.fetch(window.endpoint + '/categories/' + id, {method: 'DELETE'})
    .then((response) => {
      return response.text();
    })
    .then((body) => {
      const response = JSON.parse(body);
      if(response.status || response.status === false){}
      else {
        this.loadCategories();
      }
    })
    .catch((e) => {
      console.log(e);
    })
  }

  closeSnackBar = () => {
    this.setState(update(this.state, { snackbar: {open: {$set: false}}}));
  };

  updateSearch = (e) => {
    this.setState({categoriesSearch: e.target.value}, () => {
      this.loadCategories(true);
    })
  }

  highlight = (str) => {
    if(this.state.categoriesSearch.length > 0) {
      var regex = new RegExp(this.state.categoriesSearch  , 'gi');
      return {__html: str.replace(regex, function(str) {return '<span style="color: rgb(29, 137, 228);">'+str+'</span>'})};
    } else {
      return {__html:str};
    }
  }

  sortBy = (column) => {

    let sortby;
    switch(this.state.categoriesSortBy){
      case '':
        sortby = 'ASC';
        break;
      case 'ASC':
        sortby = 'DESC';
        break;
      case 'DESC':
        sortby = '';
        break;
      default:
        break;
    }

    this.setState({categoriesSortColumn: column, categoriesSortBy: sortby}, () => {
      this.loadCategories(true);
    })
  }

  render() {

    // var filteredCategories = this.state.categories.filter( //search chorva
    //   (category) => {
		//     return category.category_name.toLowerCase().indexOf(this.state.search.toLowerCase()) !== -1;
	  //   }
    // );
    
    const pages = [];
	
    for (var i=0; i < this.state.categoriesPages; i++) {
        pages.push(<RaisedButton key={`page-${i}`} label={i + 1} primary={i === this.state.categoriesCurrentPage} onTouchTap={this.changePage.bind(this,i)} style={{minWidth: '20px'}} />);
    }
    
    return (
      
      <div style={{position: 'relative', height: '100%'}}>
        <Toolbar className="resToolbar" style={toolbarStyle}>
          <ToolbarGroup className="resMenu" firstChild={true}>
            <Menu disableAutoFocus={true}>
              <MenuItem leftIcon={<SearchIcon style={{marginRight: '0px'}}/>} style={{paddingLeft: '50px'}} disabled={true}>
                <TextField hintText='Search Category' value={this.state.categoriesSearch} onChange={this.updateSearch.bind(this)}/>
              </MenuItem>
            </Menu>
          </ToolbarGroup>

          <ToolbarGroup lastChild={true} className="resMenu">
            <RaisedButton label="Create Category" onTouchTap={this.showCreateCategoryModal} style={{margin: '10px'}} primary={true}/>
          </ToolbarGroup>
        </Toolbar>

       <Table style={{height:'100%',overflow:'hidden'}} bodyStyle={{marginBottom: '50px'}}>
          <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
            <TableRow style={tableHeaderTableRowStyle}>
              <TableHeaderColumn onTouchTap={this.sortBy.bind(this,'item_name')} style={Object.assign({}, tableHeaderColumnStyle, {cursor: 'pointer'})}>Category
              {this.state.categoriesSortColumn === 'item_name' &&
                <span>
                {this.state.categoriesSortBy === 'ASC' &&
                  <Asc color={'white'} style={{height: '13px'}}/>
                }
                {this.state.categoriesSortBy === 'DESC' &&
                  <Desc color={'white'} style={{height: '13px'}}/>
                }
                </span>
              }
              </TableHeaderColumn>
              <TableHeaderColumn onTouchTap={this.sortBy.bind(this,'date_created')} style={Object.assign({}, tableHeaderColumnStyle, {cursor: 'pointer'})}>Date Created
              {this.state.categoriesSortColumn === 'date_created' &&
                  <span>
                  {this.state.categoriesSortBy === 'ASC' &&
                    <Asc color={'white'} style={{height: '13px'}}/>
                  }
                  {this.state.categoriesSortBy === 'DESC' &&
                    <Desc color={'white'} style={{height: '13px'}}/>
                  }
                  </span>
                }
              </TableHeaderColumn>
              <TableHeaderColumn style={{...tableHeaderColumnStyle, padding: '0 24px', width: '50px'}}></TableHeaderColumn>
              <TableHeaderColumn style={{...tableHeaderColumnStyle, padding: '0 24px', width: '50px'}}></TableHeaderColumn>
              {/*<TableHeaderColumn style={{...tableHeaderColumnStyle, padding: '0 24px', width: '50px'}}></TableHeaderColumn>*/}
            </TableRow>
          </TableHeader>

           {this.state.categories.length > 0 &&
            <TableBody displayRowCheckbox={false} showRowHover={true} stripedRows={false}>

            {this.state.categories.map( (category, index) => {
                const rowClassName = index % 2 === 0 ? 'odd' : 'even';
                return <TableRow key={index} className={rowClassName}>
                <TableRowColumn><div dangerouslySetInnerHTML={this.highlight(category.category_name)} /></TableRowColumn>
                <TableRowColumn>{`${moment(category.date_created).format('MMM DD YYYY')}`} <em>{`(${moment(category.date_created).fromNow()})`}</em></TableRowColumn>

                <TableRowColumn style={{width: '50px'}}>
                    <IconButton tooltip="Edit" onTouchTap={this.showEditCategoryModal.bind(this, category.category_id)}>
                      <EditStoreIcon />
                    </IconButton>
                  </TableRowColumn>

                <TableRowColumn style={{width: '50px'}}>
                    <IconButton tooltip="Remove">
                      <RemoveStoreIcon onTouchTap={this.removeCategory.bind(this, category.category_id)}/>
                    </IconButton>
                  </TableRowColumn>
                {/*<TableRowColumn style={{width: '50px'}}>
                    <IconButton tooltip="Archive">
                      <ArchiveIcon />
                    </IconButton>
                  </TableRowColumn>*/}
              </TableRow>
            })}
          </TableBody>
           }
           {this.state.categories.length === 0 &&
            <TableBody displayRowCheckbox={false}>
              <TableRow>
                <TableRowColumn>There are no categories found</TableRowColumn>
              </TableRow>
            </TableBody>
            }
        </Table>
         
        <div style={{position: 'absolute', right: '0px', bottom: '0px'}}>
          {pages}
        </div>
        <div style={{clear: 'both'}}></div>
        
         <Snackbar
          open={this.state.snackbar.open}
          message={this.state.snackbar.message}
          autoHideDuration={4000}
          onRequestClose={this.closeSnackBar}
        />

        
        <CreateCategory open={this.state.showCreateCategory} hideCreateCategoryModal={this.hideCreateCategoryModal}/>
        <EditCategory hideEditCategoryModal={this.hideEditCategoryModal} editCategoryID={this.state.editCategoryID}/>
      </div>
    );
  }
}

import Asc from 'material-ui/svg-icons/navigation/arrow-drop-up';
import Desc from 'material-ui/svg-icons/navigation/arrow-drop-down';