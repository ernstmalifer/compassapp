import React from 'react';

import {
  Table,
  TableBody,
  TableHeader,
  TableHeaderColumn,
  TableRow,
  TableRowColumn
} from 'material-ui/Table';
import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import {Tabs, Tab} from 'material-ui/Tabs';
import TextField from 'material-ui/TextField';
import SelectField from 'material-ui/SelectField';
import MenuItem from 'material-ui/MenuItem';
import TimePicker from 'material-ui/TimePicker';
import Checkbox from 'material-ui/Checkbox';

import ChipInput from 'material-ui-chip-input'
import Dropzone from 'react-dropzone';

import { Row, Col } from 'react-flexbox-grid';

import './CreateStore.css';

const tableHeaderTableRowStyle = {
  backgroundColor: '#fff'
}

const tableHeaderColumnStyle = {
  textAlign: 'center',
  fontSize: '14px',
  color: '#616161',
  textTransform: 'uppercase',
  padding: '5px',
  height: '30px'
}

import SvgIcon from 'material-ui/SvgIcon';

const FeaturedUncheckedIcon = (props) => (
  <SvgIcon {...props}>
    <g transform="matrix(.7,0,0,.7,0,0)">
    <path d="M32 12.408l-11.056-1.607-4.944-10.018-4.944 10.018-11.056 1.607 8 7.798-1.889 11.011 9.889-5.199 9.889 5.199-1.889-11.011 8-7.798zM16 23.547l-6.983 3.671 1.334-7.776-5.65-5.507 7.808-1.134 3.492-7.075 3.492 7.075 7.807 1.134-5.65 5.507 1.334 7.776-6.983-3.671z"></path>
    </g>
  </SvgIcon>
)

const FeaturedCheckedIcon = (props) => (
  <SvgIcon {...props}>
    <g transform="matrix(.7,0,0,.7,0,0)">
    <path d="M32 12.408l-11.056-1.607-4.944-10.018-4.944 10.018-11.056 1.607 8 7.798-1.889 11.011 9.889-5.199 9.889 5.199-1.889-11.011 8-7.798z"></path>
    </g>
  </SvgIcon>
)

export default class CreateStore extends React.Component {
  state = {
    saving: false,
    open: false,
    storeCategory: [],
    imageFiles: [],
    store_trading_hours: 'daily',
    store: {store_trading_hours_daily: {}, store_trading_hours_custom: [], store_tags: [] },
  };

  store = {
    store_trading_hours_daily: {},
    store_trading_hours_custom: []
  };

  styles = {
      chip: {
        margin: 4,
      },
      wrapper: {
        display: 'flex',
        flexWrap: 'wrap',
      },
    };

  handleClose = () => {
    this.setState({open: false});
    this.props.hideCreateStoreModal();
  };

  createStore = () => {

    this.setState({saving: true});
    
    let store_trading_hours_daily = "", store_trading_hours_custom = "";

    if(this.state.store_trading_hours === 'daily'){
      store_trading_hours_daily = {opening_time: this.store.store_trading_hours_daily.opening_time.state.time, closing_time: this.store.store_trading_hours_daily.closing_time.state.time};
    } else if(this.state.store_trading_hours === 'custom'){
      store_trading_hours_custom = [

        {day: 'monday', open: this.store.store_trading_hours_custom.monday_open.isChecked(), opening_time: this.store.store_trading_hours_custom.monday_opening_time.state.time, closing_time: this.store.store_trading_hours_custom.monday_closing_time.state.time},
        {day: 'tuesday', open: this.store.store_trading_hours_custom.tuesday_open.isChecked(), opening_time: this.store.store_trading_hours_custom.tuesday_opening_time.state.time, closing_time: this.store.store_trading_hours_custom.tuesday_closing_time.state.time},
        {day: 'wednesday', open: this.store.store_trading_hours_custom.wednesday_open.isChecked(), opening_time: this.store.store_trading_hours_custom.wednesday_opening_time.state.time, closing_time: this.store.store_trading_hours_custom.wednesday_closing_time.state.time},
        {day: 'thursday', open: this.store.store_trading_hours_custom.thursday_open.isChecked(), opening_time: this.store.store_trading_hours_custom.thursday_opening_time.state.time, closing_time: this.store.store_trading_hours_custom.thursday_closing_time.state.time},
        {day: 'friday', open: this.store.store_trading_hours_custom.friday_open.isChecked(), opening_time: this.store.store_trading_hours_custom.friday_opening_time.state.time, closing_time: this.store.store_trading_hours_custom.friday_closing_time.state.time},
        {day: 'saturday', open: this.store.store_trading_hours_custom.saturday_open.isChecked(), opening_time: this.store.store_trading_hours_custom.saturday_opening_time.state.time, closing_time: this.store.store_trading_hours_custom.saturday_closing_time.state.time},
        {day: 'sunday', open: this.store.store_trading_hours_custom.sunday_open.isChecked(), opening_time: this.store.store_trading_hours_custom.sunday_opening_time.state.time, closing_time: this.store.store_trading_hours_custom.sunday_closing_time.state.time},
      ];
    }

    const store = {
      store_name: this.store.store_name.getValue(),
      store_category: this.state.storeCategory,
      store_contact_number: this.store.store_contact_number.getValue(),
      store_email_address: this.store.store_email_address.getValue(),
      store_website: this.store.store_website.getValue(),
      store_description: this.store.store_description.getValue(),
      store_tags: this.store.store_tags.state.chips.map((chip)=> {return {tag_name: chip };}),
      store_featured: this.store.store_featured.isChecked() ? '1' : '0',
      store_trading_hours: this.state.store_trading_hours,
      store_trading_hours_daily: store_trading_hours_daily,
      store_trading_hours_custom: store_trading_hours_custom,
      owner_name: this.store.owner_name.getValue(),
      owner_email: this.store.owner_email.getValue(),
      owner_contact_number: this.store.owner_contact_number.getValue(),
      eventbrite_id: this.store.eventbrite_id.getValue(),
      is_active: '1'
    }

    var data = new FormData()
    data.append('data', JSON.stringify(store));

    if(this.state.imageFiles.length > 0){
      data.append('file', this.state.imageFiles[0]);
    }
    window.fetch(window.endpoint + '/stores', {
      method: 'POST',
      body: data
    })
    .then((response) => {
      return response.text();
    })
    .then((body) => {
      this.setState({saving: false});
      this.handleClose();
    })
    .catch((error) => {
      this.setState({saving: false});
      console.log(error);
    })

  }

  handleStoreCategoryChange = (event, index, value) => this.setState({storeCategory: value});

  onDrop = (imageFiles) => {
    this.setState({
        imageFiles: imageFiles
    })
  }
    

  componentWillReceiveProps = (nextProps) => {
      this.setState({open: nextProps.open});
  }

  render() {
    const actions = [
      <FlatButton
        label="Cancel"
        primary={true}
        onTouchTap={this.handleClose}
        style={{color: '#CCC', marginRight: '10px' }}
        hoverColor="white"
      />,
      <RaisedButton label={!this.state.saving ? 'Save' : 'Saving'} onTouchTap={this.createStore} backgroundColor="#1D89E4" labelColor="#FFF" disabled={this.state.saving}/>
    ];

    return (
      <div>
        <Dialog
          title="Create Store"
          actions={actions}
          modal={true}
          className="createStoreDialog"
          open={this.state.open}
          onRequestClose={this.handleClose}
          autoScrollBodyContent={true}
          autoDetectWindowHeight={true}
          repositionOnUpdate={true}
        >
          <Tabs style={{margin: '10px 0px'}} initialSelectedIndex={0}>
            <Tab style={{backgroundColor: '', color: '#1D89E4'}} label={< span><MapsPersonPin style={{ width: '30px', verticalAlign: 'middle', color: '#1D89E4' }}></MapsPersonPin>Store Details</span>}>
                <div>
                    <Row>
                        <Col xs>
                            <TextField floatingLabelText="Store Name" floatingLabelFixed={true} fullWidth={true} ref={(input) => this.store.store_name = input}/>
                        </Col>
                        <Col xs>
                            <SelectField floatingLabelText="Category" value={this.state.storeCategory} onChange={this.handleStoreCategoryChange} fullWidth={true} multiple={true}>
                              {this.props.categories.map((category, idx)=> {
                                return <MenuItem key={`filtercategory-${category.category_id}`} value={category.category_id} primaryText={category.category_name} />
                              })}
                            </SelectField>
                        </Col>
                    </Row>
                    <Row>
                        <Col xs={6}>
                            <TextField floatingLabelText="Store Contact Number" floatingLabelFixed={true} fullWidth={true} ref={(input) => this.store.store_contact_number = input} />
                        </Col>
                        <Col xs={3}>
                            <TextField floatingLabelText="Store Email Address" floatingLabelFixed={true} fullWidth={true} ref={(input) => this.store.store_email_address = input} />
                        </Col>
                        <Col xs={3}>
                            <TextField floatingLabelText="Store Website" floatingLabelFixed={true} fullWidth={true}  ref={(input) => this.store.store_website = input} />
                        </Col>
                    </Row>
                    <Row>
                        <Col xs>
                            <TextField floatingLabelText="Description" floatingLabelFixed={true} multiLine={true} fullWidth={true} rows={1} rowsMax={4}  ref={(input) => this.store.store_description = input} />
                        </Col>
                    </Row>
                    <Row>
                        <Col xs>
                            <ChipInput floatingLabelText="Tags" floatingLabelFixed={true} fullWidth={true} hintText="Hit <enter> to add"  ref={(input) => this.store.store_tags = input} />
                        </Col>
                    </Row>
                    <Row>
                        <Col xs>
                            <span style={{color: 'rgba(0, 0, 0, 0.3)', transform: 'scale(0.75) translate(0px, 0px)', transformOrigin: 'left top 0px', display: 'block', height: '20px'}}>Store Photo</span>
                            <Dropzone
                            onDrop={this.onDrop.bind(this)} //<= Here
                            className='dropzone'
                            activeClassName='active-dropzone'
                            multiple={false}
                            >
                                {this.state.imageFiles.length > 0 &&
                                    <div>{this.state.imageFiles.map((file, idx) => <img key={`upload-${idx}`} alt="" style={{width: '200px', height: '150px', backgroundSize: 'cover', backgroundPosition: 'center', backgroundImage: `url('${file.preview}')`}} /> )}</div>
                                }
                                {this.state.imageFiles.length === 0 &&
                                    <div style={{width: '200px', border: 'solid 1px #CCC', height: '150px', color: '#CCC', fontSize: '10px'}}><div style={{padding: '20px'}}>Drag and drop or click to select file to upload.</div></div>
                                }
                            </Dropzone>
                        </Col>
                        <Col xs>
                          <span style={{color: 'rgba(0, 0, 0, 0.3)', transform: 'scale(0.75) translate(0px, 0px)', transformOrigin: 'left top 0px', display: 'block', height: '20px'}}>Featured Store</span>
                          <Checkbox checkedIcon={<FeaturedCheckedIcon />} uncheckedIcon={<FeaturedUncheckedIcon />} ref={(input) => this.store.store_featured = input} />
                        </Col>
                    </Row>
                </div>
            </Tab>
            <Tab style={{backgroundColor: '', color: '#1D89E4'}} label={< span><MapsPersonPin style={{ width: '30px', verticalAlign: 'middle', color: '#1D89E4' }}></MapsPersonPin>Trading Hours</span>}>
                    <div>
                        <div style={{textAlign: 'center', margin: '20px 0'}}>
                        <RaisedButton label="Daily" primary={this.state.store_trading_hours === 'daily'} onTouchTap={ () => { this.setState({store_trading_hours: 'daily'}) } } />
                        <RaisedButton label="Custom" primary={this.state.store_trading_hours === 'custom'} onTouchTap={ () => { this.setState({store_trading_hours: 'custom'}) } } />
                        </div>

                        {this.state.store_trading_hours === 'daily' &&
                            <div style={{textAlign: 'center', margin: '20px 0'}}>
                                <Table style={{width: '80%', margin: '0 auto'}}>
                                    <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
                                        <TableRow style={tableHeaderTableRowStyle}>
                                            <TableHeaderColumn style={tableHeaderColumnStyle}>Day</TableHeaderColumn>
                                            <TableHeaderColumn style={tableHeaderColumnStyle}>Opening Time</TableHeaderColumn>
                                            <TableHeaderColumn style={tableHeaderColumnStyle}>Closing Time</TableHeaderColumn>
                                        </TableRow>
                                    </TableHeader>
                                    <TableBody displayRowCheckbox={false}>
                                        <TableRow>
                                            <TableRowColumn>
                                                Daily
                                            </TableRowColumn>
                                            <TableRowColumn>
                                                <TimePicker hintText="Opening Time" ref={(input) => this.store.store_trading_hours_daily.opening_time = input} />
                                            </TableRowColumn>
                                            <TableRowColumn>
                                                <TimePicker hintText="Closing Time" ref={(input) => this.store.store_trading_hours_daily.closing_time = input} />
                                            </TableRowColumn>
                                        </TableRow>
                                    </TableBody>
                            </Table>
                            </div>
                        }
                        {this.state.store_trading_hours === 'custom' &&
                            <div style={{textAlign: 'center', margin: '20px 0'}}>
                                <Table style={{width: '80%', margin: '0 auto'}}>
                                    <TableHeader displaySelectAll={false} adjustForCheckbox={false}>
                                        <TableRow style={tableHeaderTableRowStyle}>
                                            <TableHeaderColumn style={tableHeaderColumnStyle}>Day</TableHeaderColumn>
                                            <TableHeaderColumn style={tableHeaderColumnStyle}>Opening Time</TableHeaderColumn>
                                            <TableHeaderColumn style={tableHeaderColumnStyle}>Closing Time</TableHeaderColumn>
                                        </TableRow>
                                    </TableHeader>
                                    <TableBody displayRowCheckbox={false}>
                                        <TableRow>
                                            <TableRowColumn>
                                                <Checkbox label="Monday" ref={(input) => this.store.store_trading_hours_custom.monday_open = input} />
                                            </TableRowColumn>
                                            <TableRowColumn>
                                                <TimePicker hintText="Opening Time" ref={(input) => this.store.store_trading_hours_custom.monday_opening_time = input} />
                                            </TableRowColumn>
                                            <TableRowColumn>
                                                <TimePicker hintText="Closing Time" ref={(input) => this.store.store_trading_hours_custom.monday_closing_time = input} />
                                            </TableRowColumn>
                                        </TableRow>
                                         <TableRow>
                                            <TableRowColumn>
                                                <Checkbox label="Tuesday" ref={(input) => this.store.store_trading_hours_custom.tuesday_open = input} />
                                            </TableRowColumn>
                                            <TableRowColumn>
                                                <TimePicker hintText="Opening Time" ref={(input) => this.store.store_trading_hours_custom.tuesday_opening_time = input} />
                                            </TableRowColumn>
                                            <TableRowColumn>
                                                <TimePicker hintText="Closing Time" ref={(input) => this.store.store_trading_hours_custom.tuesday_closing_time = input} />
                                            </TableRowColumn>
                                        </TableRow>
                                         <TableRow>
                                            <TableRowColumn>
                                                <Checkbox label="Wednesday" ref={(input) => this.store.store_trading_hours_custom.wednesday_open = input}/>
                                            </TableRowColumn>
                                            <TableRowColumn>
                                                <TimePicker hintText="Opening Time" ref={(input) => this.store.store_trading_hours_custom.wednesday_opening_time = input} />
                                            </TableRowColumn>
                                            <TableRowColumn>
                                                <TimePicker hintText="Closing Time" ref={(input) => this.store.store_trading_hours_custom.wednesday_closing_time = input} />
                                            </TableRowColumn>
                                        </TableRow>
                                         <TableRow>
                                            <TableRowColumn>
                                                <Checkbox label="Thursday" ref={(input) => this.store.store_trading_hours_custom.thursday_open = input} />
                                            </TableRowColumn>
                                            <TableRowColumn>
                                                <TimePicker hintText="Opening Time" ref={(input) => this.store.store_trading_hours_custom.thursday_opening_time = input} />
                                            </TableRowColumn>
                                            <TableRowColumn>
                                                <TimePicker hintText="Closing Time" ref={(input) => this.store.store_trading_hours_custom.thursday_closing_time = input} />
                                            </TableRowColumn>
                                        </TableRow>
                                         <TableRow>
                                            <TableRowColumn>
                                                <Checkbox label="Friday" ref={(input) => this.store.store_trading_hours_custom.friday_open = input} />
                                            </TableRowColumn>
                                            <TableRowColumn>
                                                <TimePicker hintText="Opening Time" ref={(input) => this.store.store_trading_hours_custom.friday_opening_time = input} />
                                            </TableRowColumn>
                                            <TableRowColumn>
                                                <TimePicker hintText="Closing Time" ref={(input) => this.store.store_trading_hours_custom.friday_closing_time = input} />
                                            </TableRowColumn>
                                        </TableRow>
                                         <TableRow>
                                            <TableRowColumn>
                                                <Checkbox label="Saturday" ref={(input) => this.store.store_trading_hours_custom.saturday_open = input} />
                                            </TableRowColumn>
                                            <TableRowColumn>
                                                <TimePicker hintText="Opening Time" ref={(input) => this.store.store_trading_hours_custom.saturday_opening_time = input} />
                                            </TableRowColumn>
                                            <TableRowColumn>
                                                <TimePicker hintText="Closing Time" ref={(input) => this.store.store_trading_hours_custom.saturday_closing_time = input} />
                                            </TableRowColumn>
                                        </TableRow>
                                         <TableRow>
                                            <TableRowColumn>
                                                <Checkbox label="Sunday" ref={(input) => this.store.store_trading_hours_custom.sunday_open = input} />
                                            </TableRowColumn>
                                            <TableRowColumn>
                                                <TimePicker hintText="Opening Time" ref={(input) => this.store.store_trading_hours_custom.sunday_opening_time = input} />
                                            </TableRowColumn>
                                            <TableRowColumn>
                                                <TimePicker hintText="Closing Time" ref={(input) => this.store.store_trading_hours_custom.sunday_closing_time = input} />
                                            </TableRowColumn>
                                        </TableRow>
                                    </TableBody>
                            </Table>
                            </div>
                        }

                    </div>
            </Tab>
            <Tab style={{backgroundColor: '', color: '#1D89E4'}} label={< span><MapsPersonPin style={{ width: '30px', verticalAlign: 'middle', color: '#1D89E4' }}></MapsPersonPin>Store Owner Details</span>}>
                    <div>
                        <Row>
                        <Col xs>
                            <TextField floatingLabelText="Store Owner Name" floatingLabelFixed={true} fullWidth={true} ref={(input) => this.store.owner_name = input} />
                        </Col>
                        <Col xs>
                            <TextField floatingLabelText="Store Owner E-Mail Address" floatingLabelFixed={true} fullWidth={true} ref={(input) => this.store.owner_email = input} />
                        </Col>
                    </Row>
                    <Row>
                        <Col xs>
                            <TextField floatingLabelText="Store Owner Contact Number" floatingLabelFixed={true} fullWidth={true} ref={(input) => this.store.owner_contact_number = input} />
                        </Col>
                        <Col xs>
                        <TextField floatingLabelText="Eventbrite ID" floatingLabelFixed={true} fullWidth={true} ref={(input) => this.store.eventbrite_id = input} />
                        </Col>
                    </Row>
                    </div>
            </Tab>
        </Tabs>
        </Dialog>
      </div>
    );
  }
}

import MapsPersonPin from 'material-ui/svg-icons/maps/person-pin';