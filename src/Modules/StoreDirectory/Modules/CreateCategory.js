import React from 'react';


import Dialog from 'material-ui/Dialog';
import FlatButton from 'material-ui/FlatButton';
import RaisedButton from 'material-ui/RaisedButton';
import TextField from 'material-ui/TextField';

import Dropzone from 'react-dropzone';

import { Row, Col } from 'react-flexbox-grid';

import './CreateStore.css';

export default class CreateCategory extends React.Component {
  state = {
    open: this.props.open,
    imageFiles: [],
    imageFiles2: [],
  };

  category = {
  };

  handleOpen = () => {
    this.setState({open: true});
  };

  handleClose = () => {
    this.setState({open: false, imageFiles: [], imageFiles2: []});
    this.props.hideCreateCategoryModal();
  };

  onDrop = (imageFiles) => {
    this.setState({
        imageFiles: imageFiles
    })
    console.log(imageFiles)  
  }

  onDrop2 = (imageFiles) => {
    this.setState({
        imageFiles2: imageFiles
    })
    console.log(imageFiles)  
  }

  componentWillReceiveProps = (nextProps) => {
      this.setState({open: nextProps.open});
  }

  createCategory = () => {

    const category = {
      category_name: this.category.category_name.getValue(),
      description: this.category.category_description.getValue(),
      is_active: '1'
    }

    var data = new FormData()
    data.append('data', JSON.stringify(category));

    if(this.state.imageFiles.length > 0){
      data.append('file', this.state.imageFiles[0]);
    }

    if(this.state.imageFiles2.length > 0){
      data.append('file2', this.state.imageFiles2[0]);
    }

    window.fetch(window.endpoint + '/categories', {
      method: 'POST',
      body: data
    })
    .then((response) => {
      return response.text();
    })
    .then((body) => {
      this.handleClose();
    })
    .catch((error) => {
      console.log(error);
    })

  }

  render() {
    const actions = [
      <FlatButton
        label="Cancel"
        primary={true}
        onTouchTap={this.handleClose}
        style={{color: '#CCC', marginRight: '10px' }}
        hoverColor="white"
      />,
      <RaisedButton label="Add" onTouchTap={this.createCategory} backgroundColor="#1D89E4" labelColor="#FFF"/>
    ];

    return (
      <div>
        <Dialog title="Add Store Category"  
          actions={actions}
          modal={true}
          className="createCategoryDialog"
          open={this.state.open}
          onRequestClose={this.handleClose}
          autoScrollBodyContent={true}
          autoDetectWindowHeight={true}
          repositionOnUpdate={true}
        >
        
        <Row>
            <Col xs>
                <TextField floatingLabelText="Category Name" floatingLabelFixed={true} fullWidth={true} ref={(input) => this.category.category_name = input}/>
            </Col>
        </Row>

        <Row>
            <Col xs>
                <span style={{color: 'rgba(0, 0, 0, 0.3)', transform: 'scale(0.75) translate(0px, 0px)', transformOrigin: 'left top 0px', display: 'block', height: '20px', paddingBottom: '10px'}}>Upload Image</span>
                <Dropzone
                onDrop={this.onDrop.bind(this)} //<= Here
                className='dropzone'
                activeClassName='active-dropzone'
                multiple={false}
                >
                    {this.state.imageFiles.length > 0 &&
                        <div>{this.state.imageFiles.map((file, idx) => <img key={`upload-${idx}`} alt="" style={{width: '200px', height: '150px', backgroundSize: 'cover', backgroundPosition: 'center', backgroundImage: `url('${file.preview}')`}} /> )}</div>
                    }
                    {this.state.imageFiles.length === 0 &&
                        <div style={{width: '200px', border: 'solid 1px #CCC', height: '150px', color: '#363636', fontSize: '10px'}}><div style={{padding: '20px'}}>Drag and drop or browse files.</div></div>
                    }
                </Dropzone>
            </Col>
        </Row>

        <Row>
            <Col xs>
                <span style={{marginTop: '20px', color: 'rgba(0, 0, 0, 0.3)', transform: 'scale(0.75) translate(0px, 0px)', transformOrigin: 'left top 0px', display: 'block', height: '20px', paddingBottom: '10px'}}>Upload Logo</span>
                <Dropzone
                onDrop={this.onDrop2.bind(this)} //<= Here
                className='dropzone'
                activeClassName='active-dropzone'
                multiple={false}
                >
                    {this.state.imageFiles2.length > 0 &&
                        <div>{this.state.imageFiles2.map((file, idx) => <img key={`upload-${idx}`} alt="" style={{width: '200px', height: '150px', backgroundSize: 'cover', backgroundPosition: 'center', backgroundImage: `url('${file.preview}')`}} /> )}</div>
                    }
                    {this.state.imageFiles2.length === 0 &&
                        <div style={{width: '200px', border: 'solid 1px #CCC', height: '150px', color: '#363636', fontSize: '10px'}}><div style={{padding: '20px'}}>Drag and drop or browse files.</div></div>
                    }
                </Dropzone>
            </Col>
        </Row>

        <Row>
            <Col xs>
                <TextField floatingLabelText="Description" floatingLabelFixed={true} fullWidth={true} multiLine={true} rows={1} rowsMax={4} ref={(input) => this.category.category_description = input}/>
            </Col>
        </Row>
        </Dialog>
      </div>
    );
  }
}
