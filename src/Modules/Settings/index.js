import React from 'react';

import {
  Route
} from 'react-router-dom';

import Settings from './Settings';

import {Card, CardText} from 'material-ui/Card';

export default class StoreDirectory extends React.Component {

  render() {
    return (
      <Card className="MainCard">
        <CardText style={{height: 'inherit', position: 'relative', padding: '0px'}}>
        <Route exact path={`${this.props.match.url}/`} component={Settings} />
      </CardText>
      </Card>
    );
  }
}