import 'whatwg-fetch';

const APIEndpoint = 'http://54.206.38.223:5002/api/1.0/'
const accesskey = '29t6C2U9yZ2H8fPGUa56Sg3sJNutVYm8'

// const username = 'application';
// const password = '123456';

export const Authentication = {
  isAuthenticated: true,
  token: null,
  authenticate(auth, cb) {
    this.isAuthenticated = true

    window.fetch(APIEndpoint + 'session', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        'Accept': 'application/json',
        'accesskey': accesskey
      },
      body: JSON.stringify(auth)
    })
    .then(function(response) {
      return response.text()
    }).then(function(body) {
      cb(body);
    })

  },
  signout(cb) {
    this.isAuthenticated = false
    setTimeout(cb, 2000)
  }
}