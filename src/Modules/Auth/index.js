import React from 'react';

import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';

const muiTheme = getMuiTheme({
  palette: {
    primary1Color: '#1D89E4',
    primary2Color: '#1D89E4',
    accent1Color: '#1D89E4',
    pickerHeaderColor: '#1D89E4',
  },
  tabs: {
    backgroundColor: '#FFFFFF',
    textColor: '#1D89E4'
  },
  appBar: {
    height: 50,
  },
  inkBar: {
      backgroundColor: '#1D89E4'
    },
    menuItem: {
      selectedTextColor: '#1D89E4'
    },
  raisedButton: {
    primaryColor: '#1D89E4'
  }
});

import Login from './Modules/Login';
import Logout from './Modules/Logout';
// import RequestPassword from './RequestPassword';
// import Verify from './Verify';


import {
  Route
} from 'react-router-dom';

export default class FloorPlan extends React.Component {

  render() {
    return (
      <MuiThemeProvider muiTheme={muiTheme}>
        <div className="Auth">
          <Route exact path={`${this.props.match.url}`} component={Login} />
          <Route path={`${this.props.match.url}/login`} component={Login} />
          <Route path={`${this.props.match.url}/logout`} component={Logout} />
        </div>
      </MuiThemeProvider>
    );
  }
}