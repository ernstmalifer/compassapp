import React from 'react';

import { Authentication } from '../Authentication';

import {
  Redirect
} from 'react-router-dom';

import { Row, Col } from 'react-flexbox-grid';

import {
  Card,
  CardActions,
  CardTitle,
  CardText
} from 'material-ui/Card';

import TextField from 'material-ui/TextField';

import RaisedButton from 'material-ui/RaisedButton';

import './Login.css';

export default class Login extends React.Component {

  constructor(props){
    super(props);

    this.state = {
      logging: false,
      response: {},
      redirectToReferrer: false
    }
  }

  login = () => {
    this.setState({logging: true});
    Authentication.authenticate({username: this.refs.username.getValue(), password: this.refs.password.getValue()}, (body) => {
      this.setState({logging: false});
      const resp = JSON.parse(body);
      console.log(resp);
      this.setState({response: resp});
      if(resp.success){
        this.setState({redirectToReferrer: true});
      }
    })
  }

  render() {

    const { from } = this.props.location.state || {from: {pathname: '/'}};
    const { redirectToReferrer } = this.state;

    if(redirectToReferrer){
      return ( <Redirect to={from} /> )
    }

    return (
      <Row  style={{height: '100vh'}}>
        <Col id="LoginCard" xs={12} sm={8} md={6} lg={4} xsOffset={0} smOffset={2} mdOffset={3} lgOffset={4}>
          <Card >
            <CardTitle title="Sign In" subtitle="to continue to Compass"/>
              <form>
            <CardText>
              {this.state.response &&
                <div>{this.state.response.msg}</div>
              }
                    <TextField
              defaultValue=""
              floatingLabelText="Username"
              floatingLabelFixed={true}
              fullWidth={true}
              name="username"
              ref="username"
            /><br />
            <TextField
              defaultValue=""
              floatingLabelText="Your Password"
              type="password"
              floatingLabelFixed={true}
              fullWidth={true}
              name="password"
              ref="password"
              />
            </CardText>
            <CardActions style={{padding: '20px'}}>
              <RaisedButton type="submit" disabled={this.state.logging ? true : false} label={this.state.logging ? 'Logging You In' : 'Log In'} backgroundColor="#424242" labelColor="#ffffff" onClick={this.login} />
            </CardActions>
            </form>
          </Card>
        </Col>
      </Row>
    );
  }
}
