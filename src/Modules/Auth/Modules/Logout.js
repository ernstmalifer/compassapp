import React from 'react';

import { Authentication } from '../Authentication';

import {
  Redirect
} from 'react-router-dom';

import { Row, Col } from 'react-flexbox-grid';


export default class Login extends React.Component {
  constructor(props){
    super(props);

    this.state = {
      logout: false
    }
    Authentication.signout(() => {
      this.setState({logout: true});
    });
  }

  render(){
    return (
      <Row>
        <Col xs={12} sm={12} md={12} lg={12}>
        {this.state.logout && 
          <Redirect to="/" />
        }
        {!this.state.logout && 
          <h3>Logging Out</h3>
        }
      </Col>
      </Row>
    )
  }
}