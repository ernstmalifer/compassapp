import React, { Component } from 'react';

import { Authentication } from './Modules/Auth/Authentication';

import './App.css';

import { Row, Col } from 'react-flexbox-grid';

import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider';
import getMuiTheme from 'material-ui/styles/getMuiTheme';
import Drawer from 'material-ui/Drawer';

const muiTheme = getMuiTheme({
  palette: {
    primary1Color: '#1D89E4',
    primary2Color: '#1D89E4',
    accent1Color: '#1D89E4',
    pickerHeaderColor: '#1D89E4',
  },
  tabs: {
    backgroundColor: '#FFFFFF',
    textColor: '#1D89E4'
  },
  appBar: {
    height: 50,
  },
  inkBar: {
      backgroundColor: '#1D89E4'
    },
    menuItem: {
      selectedTextColor: '#1D89E4'
    },
  raisedButton: {
    primaryColor: '#1D89E4'
  }
});

import CompassAppbar from './Components/CompassAppbar.js';
import DrawerComp from './Components/DrawerComp.js';

import {
  Redirect,
  Route
} from 'react-router-dom';

import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as AppBarActions from './_actions/appbar';

import StoreDirectory from './Modules/StoreDirectory/';
import FloorPlan from './Modules/FloorPlan/';
import Reports from './Modules/Reports/';
import Settings from './Modules/Settings/';

class App extends Component {

  constructor(props){
    super(props);

    this.state = {
      width: window.innerWidth,
      height: window.innerHeight,
      appbar: this.props.appbar,
      authenticated: Authentication.isAuthenticated
    }

  }

  toggleDrawer = () => {
    this.props.toggleDrawer(!this.state.appbar.drawer);
  }

  updateDimensions = () => {
      this.setState({width: window.innerWidth, height: window.innerHeight});
      if(window.innerWidth >= 640){
        this.setState({drawer: false});
      }
  }

  componentWillReceiveProps(nextProps){
    this.setState({appbar: nextProps.appbar});
  }

  componentDidMount() {
      window.addEventListener("resize", this.updateDimensions);
  }

  render() {

    if (!this.state.authenticated) {
      return (
        <Redirect to="/auth" push/>
      )
    }

    return (
        <MuiThemeProvider muiTheme={muiTheme}>
        <div className="App">
          <CompassAppbar toggleDrawer={this.toggleDrawer} />
            <Row style={{flex: 1}}>
              {this.state.width <= 640 &&
                <Drawer open={this.state.appbar.drawer} width={300} docked={false}onRequestChange={(open) => { this.toggleDrawer(); }}>
                  <div id="DrawerComp">
                  <DrawerComp toggleDrawer={this.toggleDrawer} drawer={this.state.appbar.drawer}/>
                  </div>
                </Drawer>
              }
              {this.state.width >= 640 && this.state.appbar.drawer &&
                <Col id="DrawerComp-Inset" style={{flex: '0 0 250px'}}>
                  <DrawerComp drawer={this.state.appbar.drawer}/>
                </Col>
              }
              <Col className="AppContainer" style={{flex: 1, overflow: 'auto'}}>
                <div className="AppContainer-inner">
                  {/* check default for role */}
                  <Route exact path={`${this.props.match.url}`} component={StoreDirectory} />
                  <Route path={`${this.props.match.url}/storedirectory`} component={StoreDirectory} />
                  <Route path={`${this.props.match.url}/floorplan`} component={FloorPlan}/>
                  <Route path={`${this.props.match.url}/reports`} component={Reports}/>
                  <Route path={`${this.props.match.url}/settings`} component={Settings}/>
                </div>
              </Col>
              </Row>
        </div>
        </MuiThemeProvider>
    );
  }
}

const mapStateToProps = (state, prop) => {
  return {
    appbar: state.appbar
  }
}

const mapDispatchToProps = (dispatch) => {
  let actions = bindActionCreators( AppBarActions, dispatch);
  return { ...actions, dispatch };
}

export default connect(mapStateToProps,mapDispatchToProps)(App)