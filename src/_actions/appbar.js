const toggleDrawer = (toggle) => {
  return {
    type: 'toggleDrawer',
    toggle
  }
}
export {toggleDrawer};