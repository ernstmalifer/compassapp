import appbar from './appbar';
import { combineReducers } from 'redux';

const rootReducer = combineReducers({
    appbar
});

export default rootReducer;