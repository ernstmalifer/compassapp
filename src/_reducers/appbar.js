export default(state = { drawer: true }, payload) => {

  switch(payload.type) {
    case 'toggleDrawer':
      return {...state, drawer: payload.toggle};
    default:
      return state;
  }
};