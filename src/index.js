import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import Auth from './Modules/Auth/';
import Logout from './Modules/Auth/Modules/Logout';
import { PrivateRoute } from './Modules/Auth/PrivateRoute';

import './index.css';

import injectTapEventPlugin from 'react-tap-event-plugin';

import { Provider } from 'react-redux';
import Store from './store';

import {
  BrowserRouter as Router,
  Route,
  Link
} from 'react-router-dom';

import { Row, Col } from 'react-flexbox-grid';


import { DragDropContextProvider } from 'react-dnd';
import HTML5Backend from 'react-dnd-html5-backend';

const StoreInstance = Store();

// Needed for onTouchTap
// http://stackoverflow.com/a/34015469/988941
injectTapEventPlugin();

ReactDOM.render(

  <Provider store={StoreInstance} >
    <DragDropContextProvider backend={HTML5Backend}>
      <Router history={history}>
        <div>
          <Route exact path="/" render={
            () => 
              <Row>
                <Col xs={12} sm={12} md={12} lg={12}>
                <h1>Compass CMS Homepage</h1>
                <Link to="/app/floorplan">Go to App</Link>
                </Col>
                </Row>
          } />
          <PrivateRoute path="/app" component={App} />
          <Route path="/auth" component={Auth} />
          <Route path="/logout" component={Logout} />
        </div>
      </Router>
      </DragDropContextProvider>
  </Provider>,
  document.getElementById('root')
);