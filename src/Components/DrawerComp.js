import React from 'react';
import {List, ListItem} from 'material-ui/List';
// import Divider from 'material-ui/Divider';

import ContentInbox from 'material-ui/svg-icons/content/inbox';

import SvgIcon from 'material-ui/SvgIcon';

import update from 'immutability-helper';

import {
  NavLink
} from 'react-router-dom';

import './DrawerComp.css';

export default class DrawerComp extends React.Component {

  constructor(props){
    super(props);
    this.state = {
      expanded: [
        true,
        true,
        true
      ]
    }
  }

  handleNestedListToggle = (idx) => {
    let toupdate = update(this.state, {expanded: {$splice: [[idx,1,!this.state.expanded[idx] ]]}});
    this.setState(toupdate);
  }

  render() {
    return (
     <div className={['DrawerComp']}>
           <List>
          <ListItem
            onTouchTap={this.props.toggleDrawer}
            key={'storedirectorylistitem'}
            primaryText="Store Directory"
            open={this.state.expanded[0]}
            onNestedListToggle={this.handleNestedListToggle.bind(this,0)}
            containerElement={<NavLink to={'/app/storedirectory'} activeClassName={'active'}  />}            
            leftIcon={<ManageStoreIcon color={this.props.drawer ? 'white' : ''} />}
            nestedItems={[
              <ListItem key={'storelistlistitem'} primaryText="Store List" onTouchTap={this.props.toggleDrawer} containerElement={<NavLink exact to={'/app/storedirectory/'} />}  leftIcon={<ContentInbox color={this.props.collapsed ? '' : 'white'} />} />,
              <ListItem key={'storecategorylistitem'} primaryText="Store Category" onTouchTap={this.props.toggleDrawer} containerElement={<NavLink to={'/app/storedirectory/storecategory'} />} leftIcon={<CategoryIcon color={this.props.collapsed ? '' : 'white'} />} />
            ]}>
          </ListItem>
          <ListItem
            onTouchTap={this.props.toggleDrawer}
            key={'floorplanlistitem'}
            primaryText="Floorplans"
            open={this.state.expanded[1]}
            onNestedListToggle={this.handleNestedListToggle.bind(this,1)}
            containerElement={<NavLink to={'/app/floorplan'} activeClassName={'active'}  />}            
            leftIcon={<FloorPlanIcon color={this.props.drawer ? 'white' : ''} />}
            nestedItems={[
              <ListItem key={'assignedstoreslistitem'} primaryText="Assigned Stores" onTouchTap={this.props.toggleDrawer} containerElement={<NavLink exact to={'/app/floorplan/assignedstores'} />}  leftIcon={<ContentInbox color={this.props.collapsed ? '' : 'white'} />} />,
              <ListItem key={'amenitieslistitem'} primaryText="Amenities" onTouchTap={this.props.toggleDrawer} containerElement={<NavLink to={'/app/floorplan/amenities'} />} leftIcon={<AmenitiesIcon color={this.props.collapsed ? '' : 'white'} />} />,
              <ListItem key={'kioskslistitem'} primaryText="Kiosks" onTouchTap={this.props.toggleDrawer} containerElement={<NavLink to={'/app/floorplan/kiosks'} />} leftIcon={<KioskIcon color={this.props.collapsed ? '' : 'white'} />} />,
              /* <ListItem key={'eventslistitem'} primaryText="Events" onTouchTap={this.props.toggleDrawer} containerElement={<NavLink to={'/app/floorplan/events'} />} leftIcon={<EventIcon color={this.props.collapsed ? '' : 'white'} />} /> */
            ]}>
          </ListItem>
          {/*
          <ListItem
            onTouchTap={this.props.toggleDrawer}
            key={'reportslist'}
            primaryText="Reports"
            open={this.state.expanded[2]}
            onNestedListToggle={this.handleNestedListToggle.bind(this,2)}
            containerElement={<NavLink to={'/app/reports'} activeClassName={'active'}  />}            
            leftIcon={<ReportsIcon color={this.props.drawer ? 'white' : ''} />}
            nestedItems={[
              <ListItem key={1} primaryText="Usage" onTouchTap={this.props.toggleDrawer} containerElement={<NavLink  to={'/app/reports/usage'} />}  leftIcon={<ContentInbox color={this.props.collapsed ? '' : 'white'} />} />,
              <ListItem key={2} primaryText="Errors" onTouchTap={this.props.toggleDrawer} containerElement={<NavLink to={'/app/reports/errors'} />} leftIcon={<BugIcon color={this.props.collapsed ? '' : 'white'} />} />
            ]}>
          </ListItem>
          */}
          <ListItem
            onTouchTap={this.props.toggleDrawer}
            key={'settingslist'}
            primaryText="Settings"
            containerElement={<NavLink to={'/app/settings'} activeClassName={'active'}  />}            
            leftIcon={<SettingsIcon color={this.props.drawer ? 'white' : ''} />}
           >
          </ListItem>
          <ListItem
            key={'logoutlistitem'}
            primaryText="Logout"
            containerElement={<NavLink to={'/logout'} activeClassName={'active'}  />}            
            leftIcon={<LogoutIcon color={this.props.drawer ? 'white' : ''} />}
           >
          </ListItem>
          </List>
        </div>
    );
  }
}

const ManageStoreIcon = (props) => (
  <SvgIcon {...props}>
    <path
      d="M20 4H4v2h16V4zm1 10v-2l-1-5H4l-1 5v2h1v6h10v-6h4v6h2v-6h1zm-9 4H6v-4h6v4z"/>
  </SvgIcon>
)

const KioskIcon = (props) => (
  <SvgIcon {...props}>
    <path
      d="M21 4H3c-1.1 0-2 .9-2 2v12c0 1.1.9 2 2 2h18c1.1 0 1.99-.9 1.99-2L23 6c0-1.1-.9-2-2-2zm-2 14H5V6h14v12z"/>
  </SvgIcon>
)

const CategoryIcon = (props) => (
  <SvgIcon {...props}>
    <polygon
      points="20,20 20,12 12,12 12,14 8,14 8,10 10,10 10,2 2,2 2,10 6,10 6,26 12,26 12,30 20,30 20,22 12,22 12,24.001 8,24    8,16 12,16 12,20  "/>
  </SvgIcon>
)

/*const EventIcon = (props) => (
  <SvgIcon {...props}>
    <g transform="matrix(1.3,0,0,1.3,0,0)">
      <path
        d="M13 9H9v4h4V9zm2-7h-1V1h-2v1H6V1H4v1H3c-.55 0-1 .45-1 1v12c0 .55.45 1 1 1h12c.55 0 1-.45 1-1V3c0-.55-.45-1-1-1zm-1 12H4V6h10v8z"/>
    </g>
  </SvgIcon>
)*/

const FloorPlanIcon = (props) => (
  <SvgIcon {...props}>
    <g transform="matrix(.45,0,0,.45,-2,-3)">
      <g transform="translate(33.25,53.186)">
        <path
          d="m 0,0 14.279,-14.278 -4.787,-4.787 -7.371,7.371 -6.907,6.908 z m -14.278,-28.094 4.786,4.786 L 2.393,-35.193 4.786,-37.586 0,-42.372 Z M 0,-28.557 l -0.232,0.232 -7.139,7.139 7.371,7.371 7.371,-7.371 z m 11.613,7.371 4.786,4.787 4.788,-4.787 -14.28,-14.278 -4.786,4.786 z m -18.52,14.278 4.786,-4.786 -7.371,-7.371 v 0.001 l -6.908,-6.909 -4.787,4.787 z M 25.429,-21.186 0,4.242 -6.907,-2.665 v 0 L -25.429,-21.186 0,-46.614 l 6.907,6.907 v -10e-4 z"/>
      </g>
    </g>
  </SvgIcon>
)

/*const BugIcon = (props) => (
  <SvgIcon {...props}>
    <path
      d="M20 8h-2.81c-.45-.78-1.07-1.45-1.82-1.96L17 4.41 15.59 3l-2.17 2.17C12.96 5.06 12.49 5 12 5c-.49 0-.96.06-1.41.17L8.41 3 7 4.41l1.62 1.63C7.88 6.55 7.26 7.22 6.81 8H4v2h2.09c-.05.33-.09.66-.09 1v1H4v2h2v1c0 .34.04.67.09 1H4v2h2.81c1.04 1.79 2.97 3 5.19 3s4.15-1.21 5.19-3H20v-2h-2.09c.05-.33.09-.66.09-1v-1h2v-2h-2v-1c0-.34-.04-.67-.09-1H20V8zm-6 8h-4v-2h4v2zm0-4h-4v-2h4v2z"/>
  </SvgIcon>
)*/

/*const ReportsIcon = (props) => (
  <SvgIcon {...props}>
    <path
      d="M23 8c0 1.1-.9 2-2 2-.18 0-.35-.02-.51-.07l-3.56 3.55c.05.16.07.34.07.52 0 1.1-.9 2-2 2s-2-.9-2-2c0-.18.02-.36.07-.52l-2.55-2.55c-.16.05-.34.07-.52.07s-.36-.02-.52-.07l-4.55 4.56c.05.16.07.33.07.51 0 1.1-.9 2-2 2s-2-.9-2-2 .9-2 2-2c.18 0 .35.02.51.07l4.56-4.55C8.02 9.36 8 9.18 8 9c0-1.1.9-2 2-2s2 .9 2 2c0 .18-.02.36-.07.52l2.55 2.55c.16-.05.34-.07.52-.07s.36.02.52.07l3.55-3.56C19.02 8.35 19 8.18 19 8c0-1.1.9-2 2-2s2 .9 2 2z"/>
  </SvgIcon>
)*/

const AmenitiesIcon = (props) => (
  <SvgIcon {...props}>
    <path
      d="M12 2C8.13 2 5 5.13 5 9c0 5.25 7 13 7 13s7-7.75 7-13c0-3.87-3.13-7-7-7zm0 9.5c-1.38 0-2.5-1.12-2.5-2.5s1.12-2.5 2.5-2.5 2.5 1.12 2.5 2.5-1.12 2.5-2.5 2.5z"/>
  </SvgIcon>
)

const SettingsIcon = (props) => (
  <SvgIcon {...props}>
    <g transform="matrix(.7,0,0,.7,.5,.5)">
      <path
        d="M29.181 19.070c-1.679-2.908-0.669-6.634 2.255-8.328l-3.145-5.447c-0.898 0.527-1.943 0.829-3.058 0.829-3.361 0-6.085-2.742-6.085-6.125h-6.289c0.008 1.044-0.252 2.103-0.811 3.070-1.679 2.908-5.411 3.897-8.339 2.211l-3.144 5.447c0.905 0.515 1.689 1.268 2.246 2.234 1.676 2.903 0.672 6.623-2.241 8.319l3.145 5.447c0.895-0.522 1.935-0.82 3.044-0.82 3.35 0 6.067 2.725 6.084 6.092h6.289c-0.003-1.034 0.259-2.080 0.811-3.038 1.676-2.903 5.399-3.894 8.325-2.219l3.145-5.447c-0.899-0.515-1.678-1.266-2.232-2.226zM16 22.479c-3.578 0-6.479-2.901-6.479-6.479s2.901-6.479 6.479-6.479c3.578 0 6.479 2.901 6.479 6.479s-2.901 6.479-6.479 6.479z"></path>
    </g>
  </SvgIcon>
)

const LogoutIcon = (props) => (
  <SvgIcon {...props}>
    <g transform="matrix(.7,0,0,.7,3,0)">
      <path
        d="M24 20v-4h-10v-4h10v-4l6 6zM22 18v8h-10v6l-12-6v-26h22v10h-2v-8h-16l8 4v18h8v-6z"></path>
    </g>
  </SvgIcon>
)