import React, {Component} from 'react';
import AppBar from 'material-ui/AppBar';

import Avatar from 'material-ui/Avatar';
import SvgIcon from 'material-ui/SvgIcon';

class CompassAppbar extends Component {

  render() {

    return (
        <AppBar
          iconElementLeft={<CompassIcon style={{margin: '12px', cursor: 'pointer'}} />}
          onLeftIconButtonTouchTap={this.props.toggleDrawer}
          style={{ backgroundColor: '#2C2C2C'}}
          title="Compass"
          iconElementRight={<Logged />}
        />
    );
  }
}

import IconButton from 'material-ui/IconButton';
import IconMenu from 'material-ui/IconMenu';
import MenuItem from 'material-ui/MenuItem';
import MoreVertIcon from 'material-ui/svg-icons/navigation/more-vert';

import {
  NavLink
} from 'react-router-dom';

const Logged = (props) => (
  <div>
  <Avatar src="http://www.material-ui.com/images/uxceo-128.jpg" size={30}></Avatar>
  <IconMenu
    {...props}
    iconButtonElement={
      <IconButton><MoreVertIcon color="white"/></IconButton>
    }
    targetOrigin={{horizontal: 'right', vertical: 'top'}}
    anchorOrigin={{horizontal: 'right', vertical: 'top'}}
  >
    <MenuItem
    primaryText="Account Settings"
    />
    <MenuItem
    leftIcon={<LogoutIcon color="black" />}
    primaryText="Logout"
    containerElement={<NavLink to={'/logout'} activeClassName={'active'}  />}
    />
  </IconMenu>
  </div>
);

const CompassIcon = (props) => (
  <SvgIcon {...props}>
    <g
       transform="matrix(0.024,0,0,-0.024,-2,25)">
          <g
             transform="translate(892.0737,273.3499)">
            <path
               style={{fill: '#f03f40'}}
               d="m 0,0 c -8.024,1.1 -15.863,1.093 -23.402,0.126 -11.21,-1.436 -22.455,2.291 -30.11,10.605 l -257.546,286.881 c -12.746,13.844 -11.848,35.402 2.005,48.138 l 16.647,15.029 c 19.17,17.625 6.504,49.607 -19.536,49.326 l -124.798,-1.344 c -14.578,-0.157 -26.671,-11.322 -27.99,-25.841 l -11.236,-123.725 c -2.35,-25.877 28.398,-41.071 47.526,-23.485 l 16.371,15.328 c 13.84,12.724 35.373,11.827 48.106,-2.004 L -106.359,-37.91 c 7.442,-8.083 10.668,-19.288 8.273,-30.01 -1.96,-8.774 -2.609,-18.042 -1.723,-27.592 3.91,-42.117 38.213,-75.961 80.378,-79.319 53.951,-4.297 98.702,40.116 94.973,93.959 C 72.694,-39.771 40.818,-5.597 0,0" />
          </g>
          <g
             transform="translate(153.3502,183.8765)">
            <path
               style={{fill: '#fff'}}
               d="m 0,0 c -10.216,0 -18.228,8.768 -17.309,18.942 l 55.831,618.486 c 7.61,75.837 56.99,110.994 130.738,110.994 l 606.735,-0.368 c 10.278,-0.006 18.306,-8.882 17.283,-19.11 L 777.505,571.314 753.227,304.087 c -0.932,-10.254 -9.529,-18.106 -19.826,-18.106 v 0 c -85.2,0 -152.053,73.082 -144.483,157.945 l 9.734,109.112 c 0.907,10.168 -7.103,18.924 -17.311,18.924 H 225.378 c -8.987,0 -16.492,-6.851 -17.308,-15.801 L 175.838,202.804 c -0.928,-10.18 7.086,-18.959 17.308,-18.959 h 82.958 c 92.777,0 166.26,-75.676 165.923,-166.248 C 441.991,7.828 433.872,0 424.103,0 Z" />
          </g>
        </g>
</SvgIcon>
)

const LogoutIcon = (props) => (
  <SvgIcon {...props}>
    <g transform="matrix(.7,0,0,.7,3,0)">
      <path
        d="M24 20v-4h-10v-4h10v-4l6 6zM22 18v8h-10v6l-12-6v-26h22v10h-2v-8h-16l8 4v18h8v-6z"></path>
    </g>
  </SvgIcon>
)

CompassAppbar.childContextTypes = {

}

export default CompassAppbar;