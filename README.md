# CompassCMS

This project was bootstrapped with [Create React App](https://github.com/facebookincubator/create-react-app).

### Prerequisites
- NodeJS 6+ & NPM [Download Link](https://nodejs.org/en/)
- Yarn `npm install -g yarn`

### Install Dev Dependencies
- `cd` to directory
- `yarn install`

### Start Dev
- `yarn start`

### Build
- `yarn build`